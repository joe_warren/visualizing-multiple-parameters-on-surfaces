#!/usr/bin/env python
import sys
import json
from itertools import izip_longest
from math import sin, sqrt, pow

base_size = 20.0
circle_ratio = 3

def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * n
    return izip_longest(fillvalue=fillvalue, *args)

def distance( a, b ):
    return sqrt( pow(a[0]-b[0], 2 ) + pow(a[1]-b[1], 2 ) + pow( a[2]-b[2], 2 ) )

def pointToValueSize( p, centers ):
    r = 0.5;
    dists = map( lambda x: distance( p, x ), centers )
    minimum = min( dists );
    i = float( dists.index( minimum ) )
    rad = i / len( centers )
    if( minimum < rad * base_size/circle_ratio ):
        if( i%2 == 0 ):
            r = 0.0
        else:
            r = 1.0
    return r;

def pointToValueFade( p, centers ):
    r = 1.0;
    dists = map( lambda x: distance( p, x ), centers )
    minimum = min( dists );
    if( minimum < base_size/circle_ratio ):
        i = float( dists.index( minimum ) )
        r = i / len( centers )
    return r;


def generateCenterVertices( verts ):
    centers = [];
    for p in verts:
        if all( map( lambda x: distance( x, p ) > base_size, centers ) ):
             centers.append( p );
    return centers

with open( sys.argv[1] ) as text:
    obj = json.loads( text.read() ) 
    vertexes = [(a,b,c) for a,b,c in grouper( obj['vertices'], 3 )]
    centers = generateCenterVertices( vertexes )
    data = map( lambda x: pointToValueSize( x, centers ), vertexes )
    print json.dumps({'data': data, 'error':[0]*len(data), 'centers':centers})
