#! /usr/bin/env python
from __future__ import print_function
from array import *
import sys
import os

if( len(sys.argv) == 1 ):
  print( "expected arguments?",file=sys.stderr)
  exit()

dat_file = open( sys.argv[1] )
statinfo = os.stat(sys.argv[1])

filesize = statinfo.st_size;
data = array("d")
data.fromfile( dat_file, filesize/8 )

print( '{ "data" : [' )
print( ", ".join(map(str,data) ) )
print( "]" )

if( len(sys.argv)>2 ):
  err_file = open( sys.argv[2] )
  err_statinfo = os.stat(sys.argv[2])

  err_filesize = err_statinfo.st_size;
  err = array("d")
  err.fromfile( err_file, err_filesize/8 )
  print( ', "error" : [' )
  print( ", ".join(map(str,err) ) )
  print( "]}" )
else:
  print( ', "error" : [' )
  print( "]}" )
