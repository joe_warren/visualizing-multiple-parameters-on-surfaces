#!/usr/bin/python
import cgi
import uuid
from os import path

data_directory = '/opt/survey_data'

form = cgi.FieldStorage()
if "file" not in form:
  print 'Status: 400 Client Error'
  print 'Content-Type: text/html'
  print
  print '<html><body>'
  print '<h1>400 Client Error</h1>'
  print '<br/>This page expects file data to be posted to it'
  print '</body></html>'
else:
  data = form["file"].value
  filename = path.join( data_directory, str(uuid.uuid4()) + '.json' )
  with open( filename, 'wb' ) as fb:
    fb.write( data )
  print 'Status: 204 No response'
  print
  print 'Data should have been posted'
