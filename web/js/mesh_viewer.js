Array.prototype.min = function(comparer) {

  if (this.length === 0) return null;
  if (this.length === 1) return this[0];

  comparer = (comparer || Math.min);

  var v = this[0];
  for (var i = 1; i < this.length; i++) {
    v = comparer(this[i], v);    
  }

  return v;
}

Array.prototype.max = function(comparer) {

  if (this.length === 0) return null;
  if (this.length === 1) return this[0];

  comparer = (comparer || Math.max);

  var v = this[0];
  for (var i = 1; i < this.length; i++) {
    v = comparer(this[i], v);    
  }

  return v;
}


function min_non_error(data, error) {
  if (data.length === 0) return null;
  if (data.length === 1) return data[0];

  var comparer = Math.min;

  var v = data[0];
  var j = 0;
  
  while( error[j] !== undefined && error[j] ===1.0e10 ){
    j++;
    v = data[j];
  }
  for (var i = 1; i < data.length; i++) {
    if( error[i] === undefined || error[i] <= 1.0e10-1.0 ){
      v = comparer(data[i], v);    
    }
  }

  return v;
}

function max_non_error(data, error) {
  if (data.length === 0) return null;
  if (data.length === 1) return data[0];

  var comparer = Math.max;

  var v = data[0];
  var j = 0;
  
  while( error[j] !== undefined && error[j] ===1.0e10 ){
    j++;
    v = data[j];
  }

  for (var i = 1; i < data.length; i++) {
    if( error[i] === undefined || error[i] <= 1.0e10-1.0 ){
      v = comparer(data[i], v);    
    }
  }

  return v;
}

function error(msg) {
  if (window.console) {
    if (window.console.error) {
      window.console.error(msg);
    }
    else if (window.console.log) {
      window.console.log(msg);
    }
  }
  else
  {
    alert(msg);
  }
}

function go_fullscreen(){
  var content = document.getElementById("content");
  var canvas =  document.getElementById("canvas");

  canvas.width = screen.width;
  canvas.height = screen.height;
  content.width = canvas.width;
  content.height = canvas.height;

  canvas.style.position="fixed";

  gl.viewportWidth = canvas.width;
  gl.viewportHeight = canvas.height;

  if(content.requestFullScreen){
     content.requestFullScreen();
  } else if(content.webkitRequestFullScreen){
    content.webkitRequestFullScreen();
  } else if(canvas.mozRequestFullScreen){
    content.mozRequestFullScreen();
  }
}

function is_fullscreen(){
  return ( document.mozFullScreenElement || document.mozFullscreenElement ||
           document.FullScreenElement || document.fullscreenElement ||
           document.webkitFullScreenElement || document.webkitFullscreenElement
  );
}

function on_fullscreen_change(){

  var content = document.getElementById("content");
  var canvas =  document.getElementById("canvas");

  if( is_fullscreen() ){
    canvas.width = screen.width;
    canvas.height = screen.height;
  } else {
    canvas.width = window.screen.availWidth;
    canvas.height = window.screen.availHeight;

    canvas.style.position="";
  }
  content.width = canvas.width;
  content.height = canvas.height;

  gl.viewportWidth = canvas.width;
  gl.viewportHeight = canvas.height;
}

function setup_fullscreen(){
 //window.onresize = on_fullscreen_change;
 document.addEventListener("fullscreenchange",on_fullscreen_change,false);
 document.addEventListener("webkitfullscreenchange",on_fullscreen_change,false);
 document.addEventListener("mozfullscreenchange",on_fullscreen_change,false);
 document.addEventListener("webkitFullScreenChange",on_fullscreen_change,false);
 document.addEventListener("mozFullScreenChange",on_fullscreen_change,false);
}

var gl;
function setup_gl(){
  try{
  var canvas = document.getElementById("canvas");

  gl = canvas.getContext("experimental-webgl", {
        antialias:true,
        preserveDrawingBuffer: true
       });

  gl.viewportWidth = canvas.width;
  gl.viewportHeight = canvas.height;

  window.console.log( gl.getExtension("OES_standard_derivatives") );

  }catch(e){
    error( e );
  }
  if (!gl) {
    error("Could not initialise WebGL");
  }
}


function loadShader( shaderSource, shaderType, opt_errorCallback) {
  var errFn = opt_errorCallback || error;
  var shader = gl.createShader(shaderType);
  gl.shaderSource(shader, shaderSource);
  gl.compileShader(shader);
  var compiled = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (!compiled) {
    lastError = gl.getShaderInfoLog(shader);
    errFn("Error compiling shader '" + shader + "': " + lastError);
    gl.deleteShader(shader);
    return null;
  }
  return shader;
}

function createProgram( shaders, opt_attribs, opt_locations) {
  var program = gl.createProgram();
  for (var ii = 0; ii < shaders.length; ++ii) {
    gl.attachShader(program, shaders[ii]);
  }
  if (opt_attribs) {
    for (var ii = 0; ii < opt_attribs.length; ++ii) {
      gl.bindAttribLocation(
          program,
          opt_locations ? opt_locations[ii] : ii,
          opt_attribs[ii]);
    }
  }
  gl.linkProgram(program);

  var linked = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (!linked) {
      lastError = gl.getProgramInfoLog (program);
      error("Error in program linking: " + lastError);

      gl.deleteProgram(program);
      return null;
  }
  return program;
};

function createShaderFromScript(
    scriptId, opt_shaderType, opt_errorCallback) {
  var shaderSource = "";
  var shaderType;
  var shaderScript = document.getElementById(scriptId);
  if (!shaderScript) {
    error("Error: unknown script element: " + scriptId);
  }
  shaderSource = shaderScript.text;

  if (!opt_shaderType) {
    if (shaderScript.type == "x-shader/x-vertex") {
      shaderType = gl.VERTEX_SHADER;
    } else if (shaderScript.type == "x-shader/x-fragment") {
      shaderType = gl.FRAGMENT_SHADER;
    } else if (shaderType != gl.VERTEX_SHADER && shaderType != gl.FRAGMENT_SHADER) {
      error("Error: unknown shader type");
      return null;
    }
  }

  return loadShader(
      shaderSource, opt_shaderType ? opt_shaderType : shaderType,
      opt_errorCallback);
};

function createShaderFromFile(
  filename, shaderType, finishedParam, finished) {

  $.ajax({
    url : filename,
    dataType: "text",
    cache : false,
    success : function (data) {
      finished( 
        finishedParam.concat( 
          loadShader( data, shaderType ) )
      ,data );
            }
    });
};

function setup_program_attributes( program ){
  program.positionAttrib = gl.getAttribLocation(program, "aVertexPosition");
  program.normalAttrib = gl.getAttribLocation(program, "aVertexNormal");
  program.dataAttrib = gl.getAttribLocation(program, "aFloatData");
  program.dataIIAttrib = gl.getAttribLocation(program, "aFloatDataII");
  program.uvAttrib = gl.getAttribLocation(program, "aVertexUV");

  program.samplerNormals = gl.getUniformLocation(program, "uSamplerNormals");
  program.texScale = gl.getUniformLocation(program, "uTexScale");
  program.normalScale = gl.getUniformLocation(program, "uNormalScale");

  program.samplerTexture = gl.getUniformLocation(program, "uSamplerTexture");
  program.stdTextureScale = gl.getUniformLocation(program, "uStdTextureScale");

  program.pMatrixUniform = gl.getUniformLocation(program, "uPMatrix");
  program.mvMatrixUniform = gl.getUniformLocation(program, "uMVMatrix");

  program.triplanarUniform = gl.getUniformLocation(program, "uTriplanarBool");
}

function setup_program(p, cb){
  createShaderFromFile( p.vertex, gl.VERTEX_SHADER, [],
    function( v, vtext ) {
      if( ShaderInDocumentSetting() ){
        populate_shader_html( vtext, "vertex" );
      }
      createShaderFromFile( p.fragment, gl.FRAGMENT_SHADER, v,
      function(shaders, ftext){
        if( ShaderInDocumentSetting() ){
          populate_shader_html( ftext, "fragment" );
        }
        var program = createProgram(shaders);
        program.name = p.name;
        gl.useProgram(program);
        setup_program_attributes( program );
        cb( program ); 
      });
  });
}

function handleLoadedTexture(texture) {
  try {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.bindTexture(gl.TEXTURE_2D, null);
  } catch( e ){
    error("error loading texture");
    throw e;
  }
}

function create_framebuffer( size ){
  var framebuffer = gl.createFramebuffer();
  gl.bindFramebuffer(gl.FRAMEBUFFER, framebuffer);
  framebuffer.width = size;
  framebuffer.height = size;

  var texture = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, framebuffer.width, framebuffer.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

  var renderbuffer = gl.createRenderbuffer();
  gl.bindRenderbuffer(gl.RENDERBUFFER, renderbuffer);
  gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16,
   framebuffer.width, framebuffer.height);

   gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D,   texture, 0)
  gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT,
    gl.RENDERBUFFER, renderbuffer)

  gl.bindTexture(gl.TEXTURE_2D, null);
  gl.bindRenderbuffer(gl.RENDERBUFFER, null);
  gl.bindFramebuffer(gl.FRAMEBUFFER, null);
  return framebuffer;
}

var xyz_program_option = { 
  "vertex":   "programs/compiled/xyz.vert",
  "fragment": "programs/compiled/xyz.frag"
};

var program_options = [
  { "name":     "cymk", // 0
    "vertex":   "programs/compiled/cymk.vert", 
    "fragment": "programs/compiled/cymk.frag" },
  { "name":     "bumpmap", // 1
    "vertex":   "programs/compiled/bumpmap.vert",
    "fragment": "programs/compiled/bumpmap.frag" },
  { "name":     "rgbw", // 2
    "vertex":   "programs/compiled/rgbw.vert", 
    "fragment": "programs/compiled/rgbw.frag" },
  { "name":     "luminescent texture", // 3
    "vertex":   "programs/compiled/lum_tex.vert", 
    "fragment": "programs/compiled/lum_tex.frag" },
  { "name":     "contours", // 4
    "vertex":   "programs/compiled/contours.vert", 
    "fragment": "programs/compiled/contours.frag" },
  { "name":     "specularity", // 5
    "vertex":   "programs/compiled/specularity.vert", 
    "fragment": "programs/compiled/specularity.frag" },
  { "name":     "hatching", // 6
    "vertex":   "programs/compiled/hatching.vert", 
    "fragment": "programs/compiled/hatching.frag" },
  { "name":     "xyz",  // 7
  "vertex":   "programs/compiled/xyz.vert",
  "fragment": "programs/compiled/xyz.frag"
   },
  { "name":     "uvs", // 8
  "vertex":   "programs/compiled/uvs.vert",
  "fragment": "programs/compiled/uvs.frag"
   }
];

var normalmap_options = [
  { "name":"dotted",       "url":"normalmap/dotted.png",       "scale":0.7,   "nml_scale":1.0 },
  { "name":"Africa",       "url":"normalmap/africa.png",       "scale":0.05,   "nml_scale":1.25 },
  { "name":"Pacific",       "url":"normalmap/pacific.png",       "scale":0.018,   "nml_scale":1.0 },
  { "name":"rock",         "url":"normalmap/rock.jpg",         "scale":0.004, "nml_scale":0.6 },
  { "name":"tiles",        "url":"normalmap/tiles.png",        "scale":0.2,   "nml_scale":2.5 },
  { "name":"bumpy",        "url":"normalmap/bumpy.jpg",        "scale":0.15,  "nml_scale":1.0 },
  { "name":"paper",        "url":"normalmap/paper.jpg",        "scale":0.05,  "nml_scale":1.0 },
  { "name":"pebbles",      "url":"normalmap/pebbles.jpg",      "scale":0.015, "nml_scale":1.5 },
  { "name":"brick",        "url":"normalmap/brick.jpg",        "scale":0.02,  "nml_scale":1.5 },
  { "name":"carbon-fiber", "url":"normalmap/carbon-fiber.jpg", "scale":0.08,  "nml_scale":1.2 },
  { "name":"ice",          "url":"normalmap/ice.jpg",          "scale":0.03,  "nml_scale":2.5 },
  { "name":"flat",         "url":"normalmap/flat.png",         "scale":1.0,   "nml_scale":1.0 }
];

var texture_options = [
  { "name":"dotted",             "url":"texture/dotted.png",       "scale":0.9 },
  { "name":"Perlin noise",       "url":"texture/perlin_noise.jpg",       "scale":0.02 },
  { "name":"Repeated Perlin",       "url":"texture/tiled_perlin.jpg",       "scale":0.3 },
  { "name":"uncorrelated noise", "url":"texture/uncorrelated.png",       "scale":0.005 },
  { "name":"uncorrelated blur", "url":"texture/uncorrelated_blur.png",       "scale":0.1 },
  { "name":"black", "url":"texture/black.png",       "scale":0.01 },
  { "name":"white", "url":"texture/white.png",       "scale":0.01 },
];

function triplanar_uvs( model ){
  return array_of_zeros( model.buffer.numItems * 2 );
}

function spherical_uvs( model ){
  var scale = 50.0;
  var inds = build_face_array( model.faces );
  var r = array_of_zeros( model.buffer.numItems * 2 );
  var vertex_list = build_array_from_indices( model.verts, inds, 3 );
  for( i = 0; i< model.buffer.numItems; i++ ){
    var rad = vector.abs( [ vertex_list[i*3],
                          vertex_list[i*3+1],
                          vertex_list[i*3+2] ] );
    r[i*2] = scale * 2.0  * Math.abs( Math.atan2( 
               vertex_list[i*3 + 1], vertex_list[i*3]
             )/Math.PI );
    r[i*2+1] = 2.0 * scale * Math.abs( 
                Math.acos( vertex_list[i*3+2]/rad )/Math.PI );
  }
  return r;
}

function cylindrical_uvs( model ){
  var scale = 30.0;
  var zscale = 0.5;
  var inds = build_face_array( model.faces );
  var r = array_of_zeros( model.buffer.numItems * 2 );
  var vertex_list = build_array_from_indices( model.verts, inds, 3 );
  for( i = 0; i< model.buffer.numItems; i++ ){
    var rad = vector.abs( [ vertex_list[i*3],
                          vertex_list[i*3+1],
                          vertex_list[i*3+2] ] );
    r[i*2] = scale * 2.0  * Math.abs( Math.atan2( 
               vertex_list[i*3 + 1], vertex_list[i*3]
             )/Math.PI );
    r[i*2+1] = 2.0 * zscale * vertex_list[i*3+2];
  }
  return r;
}

function builtin_uvs( model ){
  var scale = 100.0;
  var inds = build_face_uv_array( model.faces );
  var r = build_array_from_indices( model.builtin_uvs, inds, 2 );
  for( var i = 0; i < r.length; i++ ){
    r[i] *= scale;
  }
  return r;
}

var current_texmapping = 0;

var uv_options = [
  { "name":"Triplanar Projection", "triplanar":true, "f":triplanar_uvs },
  { "name":"Spherical", "f":spherical_uvs },
  { "name":"Cylindrical", "f":cylindrical_uvs },
  { "name":"Builtin", "f":builtin_uvs }
];

var normalTexture;
var texScale;
var normalScale;

var stdTextureScale;
var stdTexture; 

function initTexture ( img_src ){
  var texture = gl.createTexture();
  texture.image = new Image();
  texture.image.onload = function () {
    handleLoadedTexture(texture)
  }
  texture.image.src = img_src;
  return texture;
}

function initNormalmap( i) {
  normalTexture = initTexture( normalmap_options[i].url );
  texScale = normalmap_options[i].scale;
  normalScale = normalmap_options[i].nml_scale;
}


function initTextures( i) {
  stdTexture = initTexture( texture_options[i].url );
  stdTextureScale = texture_options[i].scale;
}

function build_array_from_indices( raw_array, indices, elements ) {
  var a = new Array(indices.length * elements );
  var i;
  var j;
  for( i=0; i<indices.length; i++ ){
    for( j = 0; j < elements; j++ ){
      a[i*elements+j] = raw_array[indices[i]*elements+j];
    }
  }
  return a;
}

function build_plain_array( full_array ) {
  var a = new Array( full_array.length/11 );
  var i;
  for( i=0; i<full_array.length/11; i++ ){
      a[i] = full_array[i*11+1];
  }
  return a;
}

function build_face_array( full_array ) {
  var a = new Array( 3 * full_array.length/14 );
  var i;
  for( i=0; i<full_array.length/14; i++ ){
      a[i*3 + 0] = full_array[i*14+1];
      a[i*3 + 1] = full_array[i*14+2];
      a[i*3 + 2] = full_array[i*14+3];
  }
  return a;
}

function build_face_normal_array( full_array ) {
  var a = new Array( 3 * full_array.length/14 );
  var i;
  for( i=0; i<full_array.length/14; i++ ){
      a[i*3 + 0] = full_array[i*14+8];
      a[i*3 + 1] = full_array[i*14+9];
      a[i*3 + 2] = full_array[i*14+10];
  }
  return a;
}

function build_face_uv_array( full_array ) {
  var a = new Array( 3 * full_array.length/14 );
  var i;
  for( i=0; i<full_array.length/14; i++ ){
      a[i*3 + 0] = full_array[i*14+5];
      a[i*3 + 1] = full_array[i*14+6];
      a[i*3 + 2] = full_array[i*14+7];
  }
  return a;
}

function load_raw_buffer( raw, data_length ){
  var buffer = gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(
    gl.ARRAY_BUFFER, 
    new Float32Array( raw ),
    gl.STATIC_DRAW);
  buffer.itemSize = data_length;
  buffer.numItems = raw.length/data_length;
  buffer.rawData = raw;
  return buffer;
}

function load_index_buffer( raw, data_length ){
  var buffer = gl.createBuffer();

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
  gl.bufferData(
    gl.ELEMENT_ARRAY_BUFFER, 
    new Uint16Array( raw ),
    gl.STATIC_DRAW);
  buffer.itemSize = data_length;
  buffer.numItems = raw.length/data_length;
  
  return buffer;
}

function load_buffer( data, indices, data_length ){
  var raw_buffer = build_array_from_indices( data, indices, data_length );
  var r = load_raw_buffer( raw_buffer, data_length );
  r.min = data.min; r.max = data.max;
  return r;
}

function load_uvs( model ){
  return load_raw_buffer( uv_options[current_texmapping].f( model ), 
         model.buffer.numItems );
}

function load_model( json ){
    var model = {};

    var inds = build_face_array( json.faces );
    model.buffer = load_buffer( json.vertices, inds,  3 );
    inds = build_face_normal_array( json.faces );
    model.v_normals = load_buffer( json.normals, inds, 3 );
    model.verts = json.vertices;
    model.norms = json.normals;
    model.faces = json.faces; 
    model.builtin_uvs = json.uvs[0];
    model.uvs = load_uvs( model );

    window.console.log( "vertices: " + json.vertices.length/3);
    window.console.log( "faces: " + json.faces.length/14);
    return model;
}

function set_key_labels( datasetname, min, max ){
  document.getElementById( "min" + datasetname ).textContent = min.toPrecision(2);
  document.getElementById( "max" + datasetname ).textContent = max.toPrecision(2);
}

function normalize_data( d, e, datasetname ){
  var max = max_non_error(d,e);
  var min = min_non_error(d,e);
  var range = max - min;
  d.min = min;
  d.max = max;
  set_key_labels( datasetname, min, max );

  for( var i = 0; i < d.length; i++ ){
    if( e[i] === undefined || e[i] <= 1.0e10 -1.0 ){
        d[i] = ( d[i] - min ) / range;
    } else {
        d[i] = -1.0e10;
    }
  }
  return d;
}

function array_of_zeros( length ){
  var r = Array( length );
  for( var i = 0; i< length; i++ ){
    r[i] = 0.0;
  }
  return r;
} 

var smooth_data;
var smooth_dataII;


function increment_smoothing_point( 
    r, json, vertexcount, use_errors, src, dst ){

  // error is a shorthand for the error of the current source point
  var error = json.error[src];
  // If the source point is valid
  if( error===undefined || error <= 1.0e10-1.0 ){
    // And the data point will not become NaN or undefined
    // TODO: determine why this is an issue
    if( json.data[src] + r.data[dst]){
      // Work out a factor to scale the current point by
      var scale = 1.0;
      if( use_errors && error!==undefined){
        scale = 1.0/(error * error);
      }
      if( error !== undefined ){
        r.error[dst] += error;
      } else {
        r.error[dst] += 1.0;
      }
      r.data[dst] += scale * json.data[src];
      vertexcount[dst] += scale;
    }
  }
  r.data.min = json.data.min;
  r.data.max = json.data.max;
}

function smoothing( json, model, use_errors ){
  var i, j, k;
  var inds = build_face_array( model.faces );
  var r = { "data":array_of_zeros(json.data.length),
            "error":array_of_zeros(json.data.length)
          };
  var vertexcount = array_of_zeros( json.data.length);
  // for each face in the model
  for( i = 0; i< inds.length; i+=3 ){
    //to  each element
    for( j = 0; j< 3; j++ ){
      //add each of the points that make up that face
      for( k = 0; k< 3; k++ ){
        if( j !== k ){
          // That isn't the point being added to
          increment_smoothing_point( 
            r, json, vertexcount, use_errors, inds[i+k], inds[i+j] );
        }
      }
    }
  }
  for( i = 0; i < r.data.length; i++ ){
    increment_smoothing_point( r, json, vertexcount, use_errors, i, i );
    increment_smoothing_point( r, json, vertexcount, use_errors, i, i );
 
    // Then divide by the sum of the scales
    if( vertexcount[i] !== 0.0 ){
      r.data[i] /= vertexcount[i];
      r.error[i] /= 2;
    } else {
      r.error[i] = 1.0e10;
    }
  }
  return r;
}

function build_smooth_data_array( json, model, smoothness, use_errors, datasetname ){
  if( smoothness <= 0 ){
    var inds = build_face_array( model.faces );
    var data = json.data; 
    if( !NormalizeBeforeSmoothingSetting() ){
      data = normalize_data( json.data, json.error, datasetname );
    } 
    return load_buffer( data, inds, 1 )
  } else {
    return build_smooth_data_array( smoothing( json, model, use_errors ), model, smoothness - 1, use_errors, datasetname );
  }
}

function build_data_array( json, model, datasetname ){
  if( NormalizeBeforeSmoothingSetting() ){
    json.data = normalize_data( json.data, json.error, datasetname );
  } 
  return build_smooth_data_array( json, model,
    SmoothingExtentSetting(),
    SmoothingWithErrorsSetting(),
    datasetname );
}

function load_data( json, model ){
  smooth_data = function() {
    model.data = build_data_array( json, model, "1" );
    return model
  }
  return smooth_data();
}

function load_dataII( json, model ){
  var inds = build_face_array( model.faces );
  smooth_dataII = function(){
    model.dataII = build_data_array( json, model, "2" );
    return model;
  }
  return smooth_dataII();
}


function build_square(){
  var square = {};
  var buffer = [
    0.0, 0.0, 0.0,
    0.0, 10.0, 0.0,
    10.0, 10.0, 0.0,
    0.0, 0.0, 0.0,
    10.0, 0.0, 0.0,
    10.0, 10.0, 0.0,
  ];
  var normals = [
    0.1, 0.1, 1.0,
    0.1, 0.1, 1.0,
    0.1, 0.1, 1.0,
    0.1, 0.1, 1.0,
    0.1, 0.1, 1.0,
    0.1, 0.1, 1.0,
  ];
  var uvs = [
    0.0, 0.0,
    0.0, 10.0,
    10.0, 10.0,
    0.0, 0.0,
    10.0, 0.0,
    10.0, 10.0,
  ];
  var dataI = [0.0, 1.0, 1.0, 0.0, 0.0, 1.0];
  var dataII = [0.0, 0.0, 1.0, 0.0, 1.0, 1.0];
  square.buffer = load_raw_buffer( buffer, 3 );
  square.v_normals = load_raw_buffer( normals, 3 );
  square.data = load_raw_buffer( dataI, 1 );
  square.dataII = load_raw_buffer( dataII, 1 );
  square.uvs = load_raw_buffer( uvs, 2 );
  return square;
}

function deg_to_rad( deg ){
  return deg*3.14159/180.0;
}

var quaternion = trackball.axis_to_quat( [ 1.0, 0.0, 0.0 ], -Math.PI/2.0 );

function resize_canvas(){
  var canvas = document.getElementById( "canvas" );
  var content = document.getElementById( "content" );

  var w = window.innerWidth;
  var h = window.innerHeight;

  canvas.width = w;
  canvas.height = h;
  content.width = w;
  content.height = h;

  if( gl ){
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
  }
}

function setup_canvas(){
  var canvas = document.getElementById( "canvas" );

  resize_canvas();

  window.onresize = resize_canvas;

  canvas.addEventListener( "mousedown", click_canvas,false );
  canvas.addEventListener("mouseup", unclick_canvas, false );
  canvas.addEventListener( "mouseenter", enter_canvas, false);
  canvas.addEventListener( "mouseleave", leave_canvas, false);
  canvas.addEventListener( "mousemove", mouse_move_canvas, false );
  canvas.addEventListener("mousewheel", scroll_canvas, false);
  canvas.addEventListener("DOMMouseScroll", scroll_canvas, false );
}

var distance = 150.0;

function scroll_canvas( e ){
  var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
  distance *= Math.pow( 2.0, delta*0.1 );
  if( distance > 400.0 ){
   distance = 400.0;
  }
  if( distance < 80.0 ){
    distance = 80.0;
  }
  e.preventDefault();
}


function nearest_vector_index( p ){
  var res = undefined;
  var i;
  var data = model.buffer.rawData;
  // Arbitrarily large
  var min = 1.0e10;
  for( i = 0; i<data.length; i +=3 ){
    var vec = [ data[i], data[i+1], data[i+2] ];
    var dist = vector.abs( vector.subtract( vec, p ) );
    if( dist < min ){
      min = dist;
      ind = i/3;
    }
  }
  return ind;
}

function hide_crosshairs(){
  document.getElementById( "crosshairs" ).style.opacity = "0.0";
}

function set_crosshairs(dataI, dataII){
  if( draw_key_enabled ){
    var marginSize = 20;
    var keySize = 120;
    var cross = document.getElementById( "crosshairs" );
    cross.style.opacity = "1.0";
    cross.style.top = "" + ((1.0-dataI) * keySize + marginSize - cross.height/2.0) + "px";
    cross.style.right = "" + ((1.0-dataII) * keySize + marginSize - cross.width/2.0 - 10) + "px";
  }
}


function set_click_position( b ){
  var pos = [
    ((b[0]/255.0) - 0.5)/0.0075,
    ((b[1]/255.0) - 0.5)/0.0075,
    ((b[2]/255.0) - 0.5)/0.0075
  ];
  hide_crosshairs();
  if( b[3] === 0 ){
    return;
  }
   SetClickPosition(pos);

   var nearest = nearest_vector_index( pos );
    
   SetNearestIndex( nearest );

   var dataI = model.data.rawData[nearest];
   var dataII = model.dataII.rawData[nearest];

   SetNearestDataValues(dataI, dataII);

   set_crosshairs(dataI, dataII);

   SettingsClick( pos, nearest, model );
}

var click = false;
function enter_canvas(e){
  document.body.style.pointerEvents = "none";
}

function leave_canvas(e){
  document.body.style.pointerEvents = "";
  unclick_canvas(e);
}

function click_canvas(e){
  click = true;
  var button =  (e.buttons!==undefined)?e.buttons:e.which;
  if( button == 1 ){
    var c = document.getElementById( "canvas" );
    var ca = $(canvas);
    var o = ca.offset();
    var x = (e.clientX-o.left);
    var y = c.height-(e.clientY-o.top);
    var buffer = new Uint8Array( 4 );
    gl.readPixels(x, y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, buffer );
    SetSwatch( buffer );
    switch_to_xyz();
    gl.readPixels(x*512.0/gl.viewportWidth, 
                  y*512/gl.viewportHeight,
      1, 1, gl.RGBA, gl.UNSIGNED_BYTE, buffer );
   set_click_position( buffer );
   switch_from_xyz()
  }

}
function unclick_canvas(e){
  click = false;
}

var oldX=0.0;
var oldY=0.0;
var savedevent;
function mouse_move_canvas(e){
  var c = document.getElementById( "canvas" );
  var ca = $(canvas);
  var o = ca.offset();
  var x = 2.0*(e.clientX-o.left)/c.width - 1.0;
  var y = 2.0*(e.clientY-o.top)/c.height - 1.0;
  if( oldX == 0 && oldY == 0 ){
    oldX = x; oldY = y;
  }

  var button =  (e.buttons!==undefined)?e.buttons:e.which;
  if( button == 1 ){
    click = true;
    var q = trackball.track( -oldX, oldY, -x , y );
    quaternion = trackball.add_quats( q, quaternion );
  }
  oldX = x; oldY = y;
}

var program;
var model;
var keySquare;
var xyz_program;
var xyz_framebuffer;

function switch_to_xyz(){
  gl.useProgram( xyz_program );
  gl.bindFramebuffer( gl.FRAMEBUFFER, xyz_framebuffer );
}

function switch_from_xyz(){
  gl.useProgram( program );
  gl.bindFramebuffer( gl.FRAMEBUFFER, null );
}

var mvMatrix = mat4.create();
var mvMatrixStack = [];
var pMatrix = mat4.create();

function mvPushMatrix() {
  var copy = mat4.create();
  mat4.set(mvMatrix, copy);
  mvMatrixStack.push(copy);
}

function mvPopMatrix() {
  if (mvMatrixStack.length == 0) {
    throw "Illegal mvPopMatrix Call";
  }
  mvMatrix = mvMatrixStack.pop();
}

function setMatrixUniforms(program){
  gl.uniformMatrix4fv(program.pMatrixUniform, false, pMatrix);
  gl.uniformMatrix4fv(program.mvMatrixUniform, false, mvMatrix);
}

function render_model( model, program ){
  if( program.positionAttrib !== -1 ){
    gl.bindBuffer(gl.ARRAY_BUFFER, model.buffer);
    gl.enableVertexAttribArray( program.positionAttrib);
    gl.vertexAttribPointer(program.positionAttrib, 3, gl.FLOAT, false, 0, 0);
  }
  if( program.normalAttrib !== -1 ){
    gl.bindBuffer(gl.ARRAY_BUFFER, model.v_normals);
    gl.enableVertexAttribArray(program.normalAttrib);
    gl.vertexAttribPointer(program.normalAttrib, 3, gl.FLOAT, false, 0, 0);
  }
  if( program.dataAttrib !== -1 ){
    gl.bindBuffer(gl.ARRAY_BUFFER, model.data);
    gl.enableVertexAttribArray(program.dataAttrib);
    gl.vertexAttribPointer(program.dataAttrib, 1, gl.FLOAT, false, 0, 0);
  }
  if( program.dataIIAttrib !== -1 ){
    gl.bindBuffer(gl.ARRAY_BUFFER, model.dataII);
    gl.enableVertexAttribArray(program.dataIIAttrib);
    gl.vertexAttribPointer(program.dataIIAttrib, 1, gl.FLOAT, false, 0, 0);
  }
  if( program.uvAttrib !== -1 ){
    gl.bindBuffer(gl.ARRAY_BUFFER, model.uvs);
    gl.enableVertexAttribArray(program.uvAttrib);
    gl.vertexAttribPointer(program.uvAttrib, 2, gl.FLOAT, false, 0, 0);
  }
  gl.activeTexture(gl.TEXTURE0);
  gl.bindTexture(gl.TEXTURE_2D, normalTexture);
  gl.uniform1i(program.samplerNormals, 0);

  gl.activeTexture(gl.TEXTURE1);
  gl.bindTexture(gl.TEXTURE_2D, stdTexture);
  gl.uniform1i(program.samplerTexture, 1);

  gl.uniform1f(program.texScale, texScale);
  gl.uniform1f(program.stdTextureScale, stdTextureScale);

  gl.uniform1f(program.normalScale, normalScale);
 
  gl.uniform1i( program.triplanarUniform,
    uv_options[current_texmapping].triplanar? 1: 0 );
 
  gl.drawArrays(gl.TRIANGLES, 0,
    model.buffer.numItems);
}

function move_and_render_item( item, program )
{
  mvPushMatrix();


    mat4.translate( mvMatrix, [0.0, 0.0, -distance] );

    var rotmatrix = trackball.build_rotmatrix( quaternion );   
    mvMatrix = mat4.multiply( mvMatrix, rotmatrix );

    setMatrixUniforms( program );
    render_model( item, program );
  mvPopMatrix();
}
var paused = true;

function draw_key(){
  var marginSize = 20;
  var keySize = 120;

  mat4.ortho(0.0, gl.viewportWidth, 0.0, gl.viewportHeight, 1.0, -1.0, pMatrix);
  mat4.identity(mvMatrix);

  mat4.translate(mvMatrix, [gl.viewportWidth, gl.viewportHeight, 0.0]);
  mat4.translate(mvMatrix, [- keySize - marginSize, -keySize - marginSize, 0.0]);
  mat4.scale(mvMatrix, [keySize/10.0, keySize/10.0, 0.0]);

  setMatrixUniforms(program);
  stdTextureScale *=2.0;
  texScale *= 1.5;
  render_model( keySquare, program );
  stdTextureScale /=2.0;
  texScale /= 1.5;

}

function draw_scene( ){

  switch_to_xyz();

  gl.viewport(0, 0, 512, 512);
  mat4.perspective(50, gl.viewportWidth / gl.viewportHeight, 0.1, 500.0, pMatrix);
  mat4.identity(mvMatrix);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  move_and_render_item( model, xyz_program );

  switch_from_xyz();

  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
  mat4.perspective(50, gl.viewportWidth / gl.viewportHeight, 0.1, 500.0, pMatrix);
  mat4.identity(mvMatrix);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  move_and_render_item( model, program );

  if( draw_key_enabled ){
    draw_key();
  }
}

var rotate = true;
function on_change_rotate(){
  rotate = RotateSetting();
}
function on_change_smoothing(){
  UpdateSmoothingExtent();
  smooth_data();
  smooth_dataII();
}

var draw_key_enabled=false;
function on_change_key(){
  var l = document.getElementById("keyLabels");
  draw_key_enabled = DrawKeySetting();
  if( draw_key_enabled ){
    l.style.display = "inherit";
  } else {
    l.style.display = "none";
  }
}

var loaded = false;
var oldTime = 0.0;
var frameCount = 0;

function tick() {
  requestAnimFrame(tick);
  if( loaded ){
    draw_scene();
    update_contour_labels();
    frameCount += 1;
  }
  var newT = new Date().getTime();
  if( !click && rotate ){
    quaternion = trackball.add_quats( 
                   trackball.axis_to_quat( [ 0.0, 1.0, 0.0 ] , 
                     Math.PI * (newT -oldTime)/8000.0
                   ),
                   quaternion
                 );
  }
  oldTime = newT;
}

function updateFpsCounter(){
  var counter = document.getElementById( "fps" );
  counter.textContent = frameCount;
  frameCount = 0;
}

var display_options = [
  { "name"  : "femur",
    "model" : "meshes/femur/femur.js",
    "datasets" : [
      {"name":"endocortical surface", "url":"meshes/femur/f_endo.js"},
      {"name":"gaussian blur",        "url":"meshes/femur/f_gauss.js"},
      {"name":"periosteal surface",   "url":"meshes/femur/f_surface.js"},
      {"name":"trabecular density",   "url":"meshes/femur/f_trab_density.js"},
      {"name":"cortical thickness",   "url":"meshes/femur/f_thickness.js"},
      {"name":"centers",              "url":"meshes/femur/f_centers.js"},
      {"name":"sizes",                "url":"meshes/femur/f_sizes.js"}
    ]
  },
  { "name"  : "left femur",
    "model" : "meshes/left_femur/left_femur.js",
    "datasets" : [
      {"name":"endocortical surface","url":"meshes/left_femur/lf_endo.js"},
      {"name":"gaussian blur",       "url":"meshes/left_femur/lf_gauss.js"},
      {"name":"periosteal surface",  "url":"meshes/left_femur/lf_surface.js"},
      {"name":"trabecular density",  "url":"meshes/left_femur/lf_trab_density.js"},
      {"name":"cortical thickness",  "url":"meshes/left_femur/lf_thickness.js"},
      {"name":"centers",             "url":"meshes/left_femur/lf_centers.js"},
      {"name":"sizes",               "url":"meshes/left_femur/lf_sizes.js"}
    ]
  },
  { "name" : "vertibrae",
    "model" : "meshes/vertibrae/vertibrae.js",
    "datasets" : [
      {"name":"endocortical surface", "url":"meshes/vertibrae/v_endo.js"},
      {"name":"gaussian blur",        "url":"meshes/vertibrae/v_gauss.js"},
      {"name":"periosteal surface",   "url":"meshes/vertibrae/v_surface.js"},
      {"name":"trabecular density",   "url":"meshes/vertibrae/v_trab_density.js"},
      {"name":"cortical thickness",   "url":"meshes/vertibrae/v_thickness.js"},
      {"name":"centers",              "url":"meshes/vertibrae/v_centers.js"},
      {"name":"sizes",                "url":"meshes/vertibrae/v_sizes.js"}
    ]
  }
];

function on_select_texmapping(){
  current_texmapping = TexMappingSetting();
  model.uvs = load_uvs( model );
}

function on_select_model(){
  var i = ModelIndexSetting();
  SetupDatasetOptions( i );
  load_model_option( i );
}

function on_select_dataset(){
  var i = FirstDataSetIndexSetting();
  var j = SecondDataSetIndexSetting();
  load_dataset_option( i );
  load_datasetII_option( j );
}

function on_select_normalmap(){
  var i = NormalmapSetting();
  initNormalmap( i );
}

function on_select_texture(){
  var i = TextureSetting;
  initTextures( i );
}

function on_select_program(){
  var i = ProgramIndexSetting();
  setup_program(program_options[i], function(p){
    program = p;
  });
}

function setup_options(){
  var models = document.getElementById("models");
  setup_named_options(models,display_options);

  SetupShaderOptions();
  SetupDatasetOptions(0);
}

var a_dataset_loaded = false;

function set_dataset_loaded(){
  if( a_dataset_loaded  ){
    loaded = true;
  }
  a_dataset_loaded = true;
}

function load_dataset_option( j ){
  var i = ModelIndexSetting();

  $.getJSON(display_options[i].datasets[j].url,{}, function(json){
    model = load_data( json, model );
    set_dataset_loaded();
  });
}

function load_datasetII_option( j ){
  var i = ModelIndexSetting();

  $.getJSON(display_options[i].datasets[j].url,{}, function(json){
    model = load_dataII( json, model );
    set_dataset_loaded();
  });
}

function load_model_option( i ){
  window.console.log("loading model: " + i );
  loaded = false;
  a_dataset_loaded = false;
  $.ajax({
    dataType: "json",
    cache: false,
    url: display_options[i].model,
    success: function(json){
      model = load_model( json );
      on_select_dataset();
    }
  });
}

var update_contour_labels;

function get_pixel_value( x, y ) { 
  var buffer = new Uint8Array( 4 );
  gl.readPixels(x, y, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, buffer );
  return [ buffer[0], buffer[1], buffer[2], buffer[3] ];
}

function get_pixel_column(  x ) { 
  var buffer = new Uint8Array( 4*gl.viewportHeight );
  gl.readPixels(x, 0, 1, gl.viewportHeight, gl.RGBA, gl.UNSIGNED_BYTE, buffer );
  return buffer;
}



function setup_contour_labels(){
  var labelCount = 20;
  var labels = [];
  var overlay = document.getElementById( "content" );
  for( var i = 0; i< labelCount; i++ ){ 
    var newLabel = document.createElement( "span" );
    newLabel.className = "contour_label";
    overlay.appendChild( newLabel );
    labels.push( newLabel ); 
  }
  update_contour_labels = function(){
    if(program.name ==="contours" ){
      if( !model.dataII ){
        return;
      }
      var min = model.dataII.min;
      var range = model.dataII.max - min;
      var x = gl.viewportWidth /2.0;
      var col = get_pixel_column(  x );
      var v;
      var l = 0;
      for( var i = 0; i < col.length && l < labelCount; i+=4 ){
        if( col[i+3] !== 0.0 ){
          if( col[i+0] === col[i+1] && col[i+1] === col[i+2] ){
            v = col[i+0];
            while ( col[i+0] === col[i+1] && col[i+1] === col[i+2] 
                    && col[i+3] !== 0.0 
                    && i <  col.length){
              i+=4;   
            }
            labels[l].textContent = (min + v * range/255).toPrecision(3);
            labels[l].style.top = "" + (gl.viewportHeight - i/4.0) +"px";
            labels[l].style.left = "" + x + "px";
            l+=1;
          }
        }
      }
      for( ; l< labelCount; l++ ){
        labels[l].textContent = "";
      }
    } else {
      labels.forEach( function( a ){
        a.textContent = "";
      });
    }
  }
}

function setup(){

  setup_canvas();

  setup_controls();

  setup_gl();

  setup_fullscreen();

  setup_options();

  setup_contour_labels();

  xyz_framebuffer = create_framebuffer(512);

  keySquare = build_square();

  OnInitSettings();

  setup_program(xyz_program_option, function(xyz_p){
    xyz_program = xyz_p;
    setup_program(program_options[ProgramIndexSetting()], function(p){
      program = p;

      initTextures( 0 );
      initNormalmap( 0 );

      on_change_rotate();
      UpdateSmoothingExtent();
      on_change_key();

      gl.clearColor(0.0, 0.0, 0.0, 0.0);
      gl.enable(gl.DEPTH_TEST);

      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
 
      load_model_option( ModelIndexSetting() );

      window.setInterval( updateFpsCounter, 1000 );

      tick();
    });
  });
}
window.onload = setup;
