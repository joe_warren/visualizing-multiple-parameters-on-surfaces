/* 
 * This JavaScript Trackball code was heavily based 
 *  on the C implementation of a trackball used by XScreensaver.
 *  
 * The port to JavaScript was done by Joe Warren, 
 *  and any bugs are probably mine. 
 *
 * The original trackball code came with the following licence,  
 *  which is probably invalidated by the port to JavaScript
 *  but it's still nice not to strip licences. 
 * Feel free to do whatever with this code, 
 *  I did.
 */ 

/*
 * (c) Copyright 1993, 1994, Silicon Graphics, Inc.
 * ALL RIGHTS RESERVED
 * Permission to use, copy, modify, and distribute this software for
 * any purpose and without fee is hereby granted, provided that the above
 * copyright notice appear in all copies and that both the copyright notice
 * and this permission notice appear in supporting documentation, and that
 * the name of Silicon Graphics, Inc. not be used in advertising
 * or publicity pertaining to distribution of the software without specific,
 * written prior permission.
 *
 * THE MATERIAL EMBODIED ON THIS SOFTWARE IS PROVIDED TO YOU "AS-IS"
 * AND WITHOUT WARRANTY OF ANY KIND, EXPRESS, IMPLIED OR OTHERWISE,
 * INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY OR
 * FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL SILICON
 * GRAPHICS, INC.  BE LIABLE TO YOU OR ANYONE ELSE FOR ANY DIRECT,
 * SPECIAL, INCIDENTAL, INDIRECT OR CONSEQUENTIAL DAMAGES OF ANY
 * KIND, OR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION,
 * LOSS OF PROFIT, LOSS OF USE, SAVINGS OR REVENUE, OR THE CLAIMS OF
 * THIRD PARTIES, WHETHER OR NOT SILICON GRAPHICS, INC.  HAS BEEN
 * ADVISED OF THE POSSIBILITY OF SUCH LOSS, HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, ARISING OUT OF OR IN CONNECTION WITH THE
 * POSSESSION, USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * US Government Users Restricted Rights
 * Use, duplication, or disclosure by the Government is subject to
 * restrictions set forth in FAR 52.227.19(c)(2) or subparagraph
 * (c)(1)(ii) of the Rights in Technical Data and Computer Software
 * clause at DFARS 252.227-7013 and/or in similar or successor
 * clauses in the FAR or the DOD or NASA FAR Supplement.
 * Unpublished-- rights reserved under the copyright laws of the
 * United States.  Contractor/manufacturer is Silicon Graphics,
 * Inc., 2011 N.  Shoreline Blvd., Mountain View, CA 94039-7311.
 *
 * OpenGL(TM) is a trademark of Silicon Graphics, Inc.
 */
/*
 * Trackball code:
 *
 * Implementation of a virtual trackball.
 * Implemented by Gavin Bell, lots of ideas from Thant Tessman and
 *   the August '88 issue of Siggraph's "Computer Graphics," pp. 121-129.
 *
 * Vector manip code:
 *
 * Original code from:
 * David M. Ciemiewicz, Mark Grossman, Henry Moreton, and Paul Haeberli
 *
 * Much mucking with by:
 * Gavin Bell
 */

var TRACKBALLSIZE = 0.8;

var trackball = {};

function tb_project_to_sphere( r, x,  y) {
    var d, t, z;

    d = Math.sqrt(x*x + y*y);
    if (d < r * 0.70710678118654752440) { /* Inside sphere */
        z = Math.sqrt(r*r - d*d);
    } else { /* On hyperbola */
        t = r / 1.41421356237309504880;
        z = t*t / d;
    }
    return z;
}

/*
 *  Given an axis and angle, compute quaternion.
 */
trackball.axis_to_quat = function( a, phi ){
  a = vector.normalize(a);
  var q = vector.scale( Math.sin(phi/2.0), a);
  q[3] = Math.cos(phi/2.0);
  return q;
}

/*
 * Given two rotations, e1 and e2, expressed as quaternion rotations,
 * figure out the equivalent single rotation and stuff it into dest.
 *
 * This routine also normalizes the result every RENORMCOUNT times it is
 * called, to keep error from creeping in.
 *
 */

var RENORMCOUNT = 97;
var add_quats_count = 0;

trackball.add_quats = function(q1, q2)
{
  var t1 = vector.scale(q2[3],q1);

  var t2 = vector.scale(q1[3],q2);

  var t3 = vector.cross(q2,q1);
  var tf = vector.add(t1,t2);
  tf = vector.add(t3,tf);
  tf[3] = q1[3] * q2[3] - vector.dot(q1,q2);

  if (++add_quats_count > RENORMCOUNT) {
    add_quats_count = 0;
    normalize_quat(tf);
  }
  return tf;
}

/*
 * Quaternions always obey:  a^2 + b^2 + c^2 + d^2 = 1.0
 * If they don't add up to 1.0, dividing by their magnitued will
 * renormalize them.
 *
 * Note: See the following for more information on quaternions:
 *
 * - Shoemake, K., Animating rotation with quaternion curves, Computer
 *   Graphics 19, No 3 (Proc. SIGGRAPH'85), 245-254, 1985.
 * - Pletinckx, D., Quaternion calculus as a basic tool in computer
 *   graphics, The Visual Computer 5, 2-13, 1989.
 */
function normalize_quat( q ){
  var mag = Math.sqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
  for (var i = 0; i < 4; i++) q[i] /= mag;
}

trackball.build_rotmatrix = function( q ){
 
    var m = new Float32Array(16);

    m[0 + 0] = 1.0 - 2.0 * (q[1] * q[1] + q[2] * q[2]);
    m[0 + 1] = 2.0 * (q[0] * q[1] - q[2] * q[3]);
    m[0 + 2] = 2.0 * (q[2] * q[0] + q[1] * q[3]);
    m[0 + 3] = 0.0;

    m[4 + 0] = 2.0 * (q[0] * q[1] + q[2] * q[3]);
    m[4 + 1] = 1.0 - 2.0 * (q[2] * q[2] + q[0] * q[0]);
    m[4 + 2] = 2.0 * (q[1] * q[2] - q[0] * q[3]);
    m[4 + 3] = 0.0;

    m[8 + 0] = 2.0 * (q[2] * q[0] - q[1] * q[3]);
    m[8 + 1] = 2.0 * (q[1] * q[2] + q[0] * q[3]);
    m[8 + 2] = 1.0 - 2.0 * (q[1] * q[1] + q[0] * q[0]);
    m[8 + 3] = 0.0;

    m[12 + 0] = 0.0;
    m[12 + 1] = 0.0;
    m[12 + 2] = 0.0;
    m[12 + 3] = 1.0;

    return m;
}

/*
 * Project an x,y pair onto a sphere of radius r OR a hyperbolic sheet
 * if we are away from the center of the sphere.
 */
trackball.track = function( p1x, p1y, p2x, p2y ){
  if (p1x == p2x && p1y == p2y) {
    /* Zero rotation */
    return [ 0.0, 0.0, 0.0, 1.0 ];
  }

  /*
   * First, figure out z-coordinates for projection of P1 and P2 to
   * deformed sphere
   */
  var p1 = [p1x,p1y,tb_project_to_sphere(TRACKBALLSIZE,p1x,p1y)];
  var p2 = [p2x,p2y,tb_project_to_sphere(TRACKBALLSIZE,p2x,p2y)];

  /*
   *  Now, we want the cross product of P1 and P2
   *  as this is the axis of rotation
   */
  var a = vector.cross( p1, p2 );

  /*
   *  Figure out how much to rotate around that axis.
   */
  var d = vector.subtract( p1, p2 );
  var t = vector.abs(d) / (2.0*TRACKBALLSIZE);

  /*
   * Avoid problems with out-of-control values...
   */
  if (t > 1.0) t = 1.0;
  if (t < -1.0) t = -1.0;
  var phi = 2.0 * Math.asin(t);

  return trackball.axis_to_quat(a,phi);
} 

