var vector = {

  "rotate" : function( vec, theta, u ){
    var r = new Array(3);
    var x = 0;
    var y = 1;
    var z = 2;

    var c = Math.cos(theta);
    var s = Math.sin(theta);
    r[x] = (c+u[x]*u[x]*(1.0-c))      * vec[x]
         + (u[x]*u[y]*(1.0-c)-u[z]*s) * vec[y]
         + (u[x]*u[z]*(1.0-c)+u[y]*s) * vec[z];

    r[y] = (u[x]*u[y]*(1.0-c)+u[z]*s) * vec[x]
         + (c+u[y]*u[y]*(1.0-c))      * vec[y]
         + (u[y]*u[z]*(1.0-c)-u[x]*s) * vec[z];

    r[z] = (u[x]*u[z]*(1.0-c)-u[y]*s) * vec[x]
         + (u[y]*u[z]*(1.0-c)+u[x]*s) * vec[y]
         + (c+u[z]*u[z]*(1-c))        * vec[z];

    return r;
  },

  "add" : function( a, b ){
    var r = new Array(3);
    r[0]=a[0]+b[0];
    r[1]=a[1]+b[1];
    r[2]=a[2]+b[2];
    return r;
  },

  "subtract" : function( a, b ){
    var r = new Array(3);
    r[0]=a[0]-b[0];
    r[1]=a[1]-b[1];
    r[2]=a[2]-b[2];
    return r;
  },

  "scale" : function( s, a ){
    var r = new Array(3);
    r[0]=s*a[0];
    r[1]=s*a[1];
    r[2]=s*a[2];
    return r;
  },

  "dot" : function( a, b ){
    r=0.0;
    r+=a[0]*b[0];
    r+=a[1]*b[1];
    r+=a[2]*b[2];
    return r;
  },

  "cross" : function( a, b ){
    r = new Array(3);
    r[0] = a[1]*b[2] - a[2]*b[1];
    r[1] = a[2]*b[0] - a[0]*b[2];
    r[2] = a[0]*b[1] - a[1]*b[0];
    return r;
  },

  "abs" : function( a ){
    return Math.sqrt( vector.dot( a, a ) );
  },

  "positive" : function( a ){
    var r = new Array(3);
    r[0] = Math.abs( a[0] );
    r[1] = Math.abs( a[1] );
    r[2] = Math.abs( a[2] );
    return r;
  },

  "normalize" : function( a ){
    return vector.scale( 1.0/vector.abs(a), a );
  }
}

