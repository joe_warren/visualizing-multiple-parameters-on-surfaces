function shade_text_in_html( text, textvalues, textclass ){
  textvalues.forEach( function(d){
    text = text.replace( d, "<span class=\"" + textclass + "\">$&</span>", "g");
  });
  return text;
}  

var populate_shader_html = function( text, divId ){

  var datatypes = [ "float", "vec2", "vec3", "vec4", "sampler2D",
    "mat2", "mat3", "mat4", "void" ];
  var keywords = [ "attribute", "uniform", "varying", "precision",
    "highp", "mediump", "lowp" ];
  var reserved = [ /gl\_[^\s]*/g ];
  var comments = [ /\/\/.*/g, /\/\* [^(\*\/)]*\*\//g ]
  var operators = [ "+", "-", "*", "&#47;", "{", "}", "(", ")"];
  var functions = [ "dot", "cross", "clamp", "texture2D", "abs", "mix", 
    "normalize"];

  var div = document.getElementById( divId );
  div.textContent = text;
  var escaped = div.innerHTML;
  escaped = shade_text_in_html( escaped, datatypes, "datatype" );
  escaped = shade_text_in_html( escaped, keywords, "keyword" );
  escaped = shade_text_in_html( escaped, reserved, "reserved" );
  escaped = shade_text_in_html( escaped, comments, "comment" );
  escaped = shade_text_in_html( escaped, operators, "operator" );
  escaped = shade_text_in_html( escaped, functions, "function" );

  div.innerHTML = "<pre>" + escaped + "</pre>";
}
