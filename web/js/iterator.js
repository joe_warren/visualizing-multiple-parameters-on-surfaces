if( Iterator === undefined ){
  if( StopIteration === undefined ){
    var StopIteration = "__STOP_ITERATION__";
  }
  var Iterator = function(arr){ return {
    index : -1,
    hasNext : function(){ return this.index < arr.length - 1; },
    hasPrevious: function(){ return this.index > 0; },

    current: function(){ return [ this.index, arr[ this["index"] ] ]; },

    next : function(){
        if(this.hasNext()){
            this.index = this.index + 1;            
            return this.current();
        } 
        throw StopIteration;
    },

    previous : function(){
        if(this.hasPrevious()){
            this.index = this.index - 1
            return this.current();
        }
        throw StopIteration;
    }
}   
};
}
