Array.prototype.shuffle = function(){
    for(var j, x, i = this.length;
        i;
        j = Math.floor(Math.random() * i), 
        x = this[--i],
        this[i] = this[j],
        this[j] = x);
    return this;
};


function NormalizeBeforeSmoothingSetting(){
  return true;
}

function SmoothingExtentSetting(){
  return 1;
}

function SmoothingWithErrorsSetting(){
  return false;
}

function SetClickPosition( pos ){
}

function SetNearestIndex( nearest ){
}

function SetNearestDataValues( dataI, dataII ){
}

function SetSwatch( c ){
}

var rotateEnabled = true;
function RotateSetting( ){
  return rotateEnabled;
}

// Called on any click, not just an object one

function SetSwatch( c ){
  if( rotateEnabled ){
    rotateEnabled = false;
    on_change_rotate();
  }
}


function UpdateSmoothingExtent(){
}

function DrawKeySetting(){
  return true;
}

function TexMappingSetting(){
 return 0;
}

var modelIndexChoices = [1, 2];
// The left femur and vertebrae objects,

var dataIndexChoices = [1, 2, 3, 4];
// Every Dataset except for the circles

var modelIndexChoice = 0;
var dataIndexIChoice = 0; 
var dataIndexIIChoice = 0;

function RandomChoice(arr){
  return arr[Math.floor(Math.random() * arr.length)];
}

function updateData(){
  dataIndexIChoice = RandomChoice( dataIndexChoices );
  do {
    dataIndexIIChoice = RandomChoice( dataIndexChoices );
  } while( dataIndexIIChoice === dataIndexIChoice );
  if( currentTest.preFunc ){
    currentTest.preFunc();
  }
  on_select_dataset();
}

function updateModelAndData(){
  modelIndexChoice = RandomChoice( modelIndexChoices );
  updateData();
  on_select_model();
}

function ModelIndexSetting(){
  return modelIndexChoice;
}

function FirstDataSetIndexSetting(){
  return dataIndexIChoice;
}

function SecondDataSetIndexSetting(){
  return dataIndexIIChoice;
}

function NormalmapSetting(){
  return 0;
}

function TextureSetting(){
  return 0;
}

var TestableProgramIndices = Iterator([ 0, 1, 3, 4 ].shuffle());
var currentTestProgram = TestableProgramIndices.next()[1];

function ProgramIndexSetting(){
  return currentTestProgram;
}

function revealFinalOverlay(){
  document.getElementById("outroText").style.display="block";
}

function PostResults(){
  $.ajax({
    type: "POST",
    url: "/cgi-bin/post_results.py",
    data: { file: JSON.stringify(resultsList, undefined, 2 ) },
    success: function(){ alert("We should never recieve a reponse") },
    dataType: "text/json"
  }).fail( function(response){
    if( response.status === 204 ){
      revealFinalOverlay();
    }
  });
}

function AdvanceProgram(){
  try{
    currentTestProgram = TestableProgramIndices.next()[1];
    on_select_program();
  } catch(e){
    if( e === StopIteration ){
      throw "End Test";
    } else {
      throw e;
    }
  }
}

function ShaderInDocumentSetting(){
  return false;
}
function setup_named_options(element,options){
}

function SetupDatasetOptions(i){
}

function SetupShaderOptions(){
}

function SettingsClick( pos, nearest, model ){
  rotateEnabled = false;
  on_change_rotate();
  if( currentTest.clickFunc ){
    currentTest.clickFunc( pos, nearest, model )
  }
}

var currentResult = {};
var resultsList = [];

function setResult(result){
  result.test = currentTest.description;
  result.program = program_options[currentTestProgram].name;
  result.model = ModelIndexSetting();
  result.dataI = FirstDataSetIndexSetting();
  result.dataII = SecondDataSetIndexSetting();

  currentResult = result;
  document.getElementById("submit").removeAttribute( "disabled" );
  hideOverlay()
}

function setResultNeeded(){
  document.getElementById("submit").setAttribute("disabled","true");
}

function findActualMaximum(model){
  var max =  Number.MIN_VALUE;
  var index = 0; 
  var d = model[currentTest.parameter].rawData;
  for( var i = 0; i< d.length; i++ ){
    if( d[i] > max ){
      max = d[i];
      index = i;
    }
  }
  return {
    "index": index,
    "value": d[i],
    "pos": [ 
      model.buffer.rawData[index*3],
      model.buffer.rawData[index*3 + 1],
      model.buffer.rawData[index*3 + 2]
    ]
  };
}

function survey_point_click(pos, nearest, model){
  var value = model[currentTest.parameter].rawData[nearest]

  var maxima = findActualMaximum( model );
  document.getElementById("surveyResult").textContent = 
   "[" + pos.map( function(a){return a.toPrecision(3)} ).join() + "]";

  setResult( { "click_position": pos,
               "nearestIndex": nearest, 
               "value": value,
               "correct_value": maxima
             } ); 
}

function createSelectNode(){
  var n = document.getElementById( "surveyResult" );

  var clickFunc = function(i){
    return function(){
      setResult( { "value": i } );
    }
  };

  n.appendChild( document.createTextNode( currentTest.poorText ) );
  for( var i =0; i< 5; i++ ){
    var input = document.createElement( "input" );
    input.type = "radio";
    input.id = i;
    input.name = "surveyRadioButton";
    input.onclick = clickFunc(i);
    n.appendChild( input );
  }
  n.appendChild( document.createTextNode( currentTest.goodText ) );
}


function createTextInputNode(){
  var n = document.getElementById( "surveyResult" );
  var input = document.createElement( "textarea" );
  input.type = "text";
  input.id = "surveyTextInput";
  input.name = "surveyText";
  input.onchange = function(){
    setResult( {
      "value": input.value
    } ); 
  };
  input.onchange()
  n.appendChild( input );
}

var sortableImages = [
  {path: "img/bumpmap.png", name: "bumpmap"},
  {path: "img/contours.png", name: "contours"},
  {path: "img/cymk.png", name: "cymk"},
  {path: "img/light_spots.png", name: "lightspots"}
];

function createOrderImageNode(){
  var n = document.getElementById( "surveyResult" );
  var b = document.createElement( "span");
  var w = document.createElement( "span");
  var bar = document.createElement("div");
  b.textContent = "best"; 
  w .textContent = "worst";
  b.style.float="left";
  w.style.float = "right";
  bar.appendChild( b );
  bar.appendChild( w );
  bar.style.clear = "both";
  bar.appendChild(document.createElement("br"));
    n.appendChild(bar);
  var sortable = document.createElement("div");
  n.appendChild( sortable );
  sortableImages.forEach( function(i){
    var d = document.createElement("div");
    d.id = i.name + "_1" ;
    d.className = "sortableImage";
    var img = document.createElement("img");
    img.src = i.path;
    img.alt = i.name;
    d.appendChild( img );
    d.appendChild( document.createElement( "br" ) );
    d.appendChild(document.createTextNode( i.name ) );
    sortable.appendChild(d);
  } ); 

  var sort = $(sortable).sortable({
    items: '> div',
    update: function() {
      setResult( {order: $(sort).sortable('serialize') });
    }
  });
  setResult( {order: $(sort).sortable('serialize') } );
}


function setDataSetIFunction( value ){
  return function(){
    dataIndexIChoice = value;
  }
}

function setDataSetIIFunction( value ){
  return function(){
    dataIndexIIChoice = value;
  }
}

var SurveyTests = [
  { description: "Click the point on the object with the highest Y Axis measurement",
    clickFunc: survey_point_click,
    parameter: "data"
  },
  { description: "Click the point on the object with the highest X Axis measurement",
    clickFunc: survey_point_click,
    parameter: "dataII"
  },
  { description: "Click the point with the faintest visible circle (circles drawn using the Y axis measurement)",
    preFunc: setDataSetIFunction( 5 ),
    clickFunc: survey_point_click,
    parameter: "dataII"
  },
  { description: "Click the point with the smallest visible circle (circles drawn using the Y axis measurement)",
    preFunc: setDataSetIFunction( 6 ),
    clickFunc: survey_point_click,
    parameter: "dataII"
  },
  { description: "Click the point with the faintest visible circle (circles drawn using the X axis value)",
    preFunc: setDataSetIIFunction( 5 ),
    clickFunc: survey_point_click,
    parameter: "dataII"
  },
  { description: "Click the point with the smallest visible circle (circles drawn using the X axis value)",
    preFunc: setDataSetIIFunction( 6 ),
    clickFunc: survey_point_click,
    parameter: "dataII"
  },
  { description: "How clear do you find this visualization?",
    poorText: "unclear",
    goodText: "clear",
    initTest: createSelectNode
  },
  { description: "How attractive do you find this visualization?",
    poorText: "unsightly",
    goodText: "attractive",
    initTest: createSelectNode
  }
];

var NonProgramTests = [
  {
    description: "Click and drag to order these images by how you liked each visualization",
    initTest: createOrderImageNode,
  },
  { description: "Do you have any comments on the visualizations?",
    initTest: createTextInputNode
  }
];

function clearNode( node ){
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

var SurveyTestsIterator = Iterator( SurveyTests );
var currentTest = SurveyTestsIterator.next()[1];

function updateSurveyText(){
  document.getElementById("surveyText" ).textContent = currentTest.description;

}

function HideSurveyBox(){
  document.getElementById("surveyBox").style.bottom = "-10em";
}

var questionNumber = 0;
function IncreaseQuestionNo(){
  questionNumber++;
  document.getElementById("questionNo").textContent = questionNumber;
}


var inEndTests = false;

function AdvanceTest(){
  rotateEnabled = true;
  on_change_rotate();
  try{
    try{
      currentTest = SurveyTestsIterator.next()[1];
      updateData();
    } catch(e){
      if( e === StopIteration ){
        updateModelAndData();
        // Try advancing the program,
        // this will fail if we've finished the program specific tests
        AdvanceProgram();
        
        //  Run all the tests on the next program
        SurveyTestsIterator = Iterator( SurveyTests );
        currentTest = SurveyTestsIterator.next()[1];
      } else {
        throw e;
      }
    }
    IncreaseQuestionNo();
    setResultNeeded();
    updateSurveyText();
    if( currentTest.initTest ){
      currentTest.initTest();
    }
  } catch(e){
    if( e === "End Test" ){
      try{
        if( inEndTests === false ){
          SurveyTestsIterator = Iterator( NonProgramTests );
          inEndTests = true; 
        }
        currentTest = SurveyTestsIterator.next()[1];
        updateData();
        setResultNeeded();
        updateSurveyText();
        if( currentTest.initTest ){
         currentTest.initTest();
        }
      } catch(e){
        if( e === StopIteration ){
          PostResults();
          HideSurveyBox()
        } else {
          throw e;
        }
      }
    } else {
      throw e;
    }
  }
}


function SubmitAnswerClick(){
  clearNode(document.getElementById("surveyResult"));
  resultsList.push( currentResult );
  AdvanceTest();
}

function hideOverlay(){
  document.getElementById("introText").style.display = "none";
}

function OnInitSettings(){
  setResultNeeded();
  IncreaseQuestionNo();
  updateModelAndData();
  updateSurveyText();
}
