function NormalizeBeforeSmoothingSetting(){
  return document.getElementById("minmaxmethod").checked;
}

function SmoothingExtentSetting(){
  return parseInt(document.getElementById("smoothing").value);
}

function SmoothingWithErrorsSetting(){
  return document.getElementById("smoothingmethod").checked;
}

function SetClickPosition( pos ){
  document.getElementById( "clickPosition" ).textContent = 
    "[" + pos[0].toPrecision(3) + 
    ", " + pos[1].toPrecision(3) +
    ", " + pos[2].toPrecision(3) + "]";
}

function SetNearestIndex( nearest ){
   document.getElementById( "nearestIndex" ).textContent = nearest;
}

function SetNearestDataValues( dataI, dataII ){
   document.getElementById( "nearestDataI" ).textContent = 
     dataI.toPrecision(3);
 
   document.getElementById( "nearestDataII" ).textContent =
     dataII.toPrecision(3);
}

function build_colour_string( c ){
  return "rgba( " + [].join.apply(c,[","]) + " )";
}

function SetSwatch( c ){
  var s = build_colour_string( c );
  var swatch = document.getElementById( "swatch" );
  swatch.style.color = s;
}

function RotateSetting( ){
  return document.getElementById("rotate").checked;
}

function UpdateSmoothingExtent(){
  var s = SmoothingExtentSetting();
  document.getElementById("smoothingextent").textContent = s;
}

function DrawKeySetting(){
  return document.getElementById("key").checked;
}

function TexMappingSetting(){
 return document.getElementById( "texmapping" ).selectedIndex;
}

function ModelIndexSetting(){
  return document.getElementById("models").selectedIndex;
}

function FirstDataSetIndexSetting(){
  return document.getElementById("datasets").selectedIndex;
}

function SecondDataSetIndexSetting(){
  return document.getElementById("datasetsII").selectedIndex;
}

function NormalmapSetting(){
  return document.getElementById("normalmaps").selectedIndex;
}

function TextureSetting(){
  return document.getElementById("textures").selectedIndex;
}

function ProgramIndexSetting(){
  return document.getElementById("program").selectedIndex;
}

function ModelIndexSetting(){
  return document.getElementById("models").selectedIndex;
}

function ShaderInDocumentSetting(){
  return true;
}

function setup_named_options(element,options){
  while( element.length > 0 ){
    element.remove( element.length - 1 );
  }
  options.forEach( function( s ) {
    var option = document.createElement("option");
    option.text = s.name;
    element.add( option );
  } );
}

function SetupDatasetOptions(i){
  var datasets = document.getElementById("datasets");
  var datasetsII = document.getElementById("datasetsII");

  setup_named_options( datasets, display_options[i].datasets );
  setup_named_options( datasetsII, display_options[i].datasets )
  datasetsII[datasetsII.length-1].selected = true;
  datasetsII.selectedIndex = display_options[i].datasets.length - 1;
}

function SetupShaderOptions(){
  var normalmaps = document.getElementById("normalmaps");
  var textures = document.getElementById("textures");
  var programs = document.getElementById("program");
  var texmapping = document.getElementById("texmapping");

  setup_named_options( normalmaps, normalmap_options );
  setup_named_options( textures, texture_options );
  setup_named_options( programs, program_options );
  setup_named_options( texmapping, uv_options );
}

function SettingsClick(){
}

function OnInitSettings(){
}
