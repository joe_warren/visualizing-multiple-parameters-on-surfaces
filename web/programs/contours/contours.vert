attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute float aFloatData;
attribute float aFloatDataII;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

varying vec3 k;

varying vec3 normal;

varying float data;
varying float dataII;

void main(void) {
  data = aFloatData;
  dataII = aFloatDataII;

  normal =  normalize( mat3(uMVMatrix) * aVertexNormal );

  gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
  k = aVertexPosition.xyz;

}

