#include "derivatives.h"
precision highp float;

varying vec3 normal;
varying vec3 Onormal;

varying vec4 position;

varying vec3 k;

varying float data;
varying float dataII;

#include "colour_map.h"

float contour_amount( float contour ){
  float f  = fract (contour * 10.0);
  float df = fwidth(contour * 10.0);
 /* float g = smoothstep(df * 1.1, df*1.5, f) *
    smoothstep(df * 0.1, df*0.5, f);*/
  float g = ( f > 0.1*df && f < 1.1*df )? 0.0: 1.0;
  if( contour< 0.0 ){
    return 0.5;
  }

  return g;
}

float contour_value( float contour ){
  contour = floor( contour * 10.0 );
  return contour/10.0;
}

void main(void) {

  float intensity = dot( normalize( vec3( 1.0, 1.0, 1.0 ) ),
                         normalize( normal ) );

  intensity = 0.4 + 0.6 *clamp(intensity, 0.0, 1.0);

  vec3 colour = colour_map( data, intensity );

  vec3 contour = vec3( 1., 1., 1. )* (dataII>0.0?contour_value(dataII):0.0);
  
  colour = mix( contour, colour, contour_amount( dataII ) );

  gl_FragColor = vec4(colour, 1.0);
}
