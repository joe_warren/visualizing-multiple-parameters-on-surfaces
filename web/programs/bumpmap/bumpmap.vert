attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute float aFloatData;
attribute float aFloatDataII;
attribute vec2 aVertexUV;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

varying vec2 uv;
varying vec3 normal;
varying vec3 tanX;
varying vec3 tanY;
varying vec3 tanZ;
varying vec3 Onormal;

varying vec4 position;
varying vec4 Oposition; 

varying vec3 U;

varying float data;
varying float dataII;

void main(void) {
  data = aFloatData;
  dataII = aFloatDataII;
  Onormal = normalize( aVertexNormal );
  normal = normalize( mat3(uMVMatrix) * Onormal );
  uv = aVertexUV;

  tanX = mat3(uMVMatrix) * vec3( Onormal.x, -Onormal.z, Onormal.y);
  tanY = mat3(uMVMatrix) * vec3( Onormal.z, Onormal.y, -Onormal.x);
  tanZ = mat3(uMVMatrix) * vec3(-Onormal.y, Onormal.x,  Onormal.z);

  position = vec4( aVertexPosition, 1.0 );
  Oposition = uMVMatrix * position;
  U = vec3( normalize( Oposition ) );
  gl_Position = uPMatrix * Oposition;
}

