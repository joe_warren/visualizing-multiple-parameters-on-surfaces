precision highp float;

uniform float uTexScale;
uniform float uStdTextureScale;
uniform float uNormalScale;

uniform bool uTriplanarBool;

varying vec2 uv;
varying vec3 normal;
varying vec3 tanX;
varying vec3 tanY;
varying vec3 tanZ;

varying vec3 Onormal;

varying vec4 position;
varying vec4 Oposition; 

varying vec3 U;

varying float data;
varying float dataII;

uniform sampler2D uSamplerTexture;
uniform sampler2D uSamplerNormals;

#include "texture.h"
#include "colour_map.h"

vec3 hsv2rgb(vec3 c) {
   vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
   vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
   return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main(void) {
  vec3 n = normalize( Onormal.xyz );
  vec3 blend_weights = abs( n );
  blend_weights = ( blend_weights - 0.2 ) * 7.;  
  blend_weights = max( blend_weights, 0.0 );
  blend_weights /= ( blend_weights.x + blend_weights.y + blend_weights.z );

  vec2 coord1 = position.yz * uTexScale;
  vec2 coord2 = position.zx * uTexScale;
  vec2 coord3 = position.xy * uTexScale;

  vec3 bump1 = texture2D( uSamplerNormals, coord1 ).rgb;  
  vec3 bump2 = texture2D( uSamplerNormals, coord2 ).rgb;  
  vec3 bump3 = texture2D( uSamplerNormals, coord3 ).rgb; 

  vec3 blended_bump = getBumpMapCoord(blend_weights);

  vec3 blended_tangent = normalize(tanX) * blend_weights.xxx +  
                         normalize(tanY) * blend_weights.yyy +  
                         normalize(tanZ) * blend_weights.zzz; 

  float normalScale = uNormalScale * (dataII>0.0?dataII:0.0);


  vec3 normalTex = blended_bump * 2.0 - 1.0;
  normalTex.xy *= normalScale;
  //normalTex.y *= -1.;
  normalTex = normalize( normalTex );
  mat3 tsb = mat3( normalize( blended_tangent ), 
                   normalize( cross( normal, blended_tangent ) ),
                   normalize( normal ) );
  vec3 finalNormal = tsb * normalTex;

  vec3 vLight = normalize( vec3( 1.0, 1.0, 1.0 ) );

  vec3 vHalf = normalize( tsb*(vLight - normalize(vec3( Oposition )) ) );

  float specular = pow( max(dot( vHalf , normalTex ), 0.0 ), 64.0 );

  float intensity = dot( vLight,
                         normalize( finalNormal ) );
  intensity = 0.2 + 0.8*clamp(intensity, 0.0, 1.0);

  vec3 colour = colour_map( data, intensity );

  colour += vec3( 0.5, 0.5, 0.5 ) * specular;

  gl_FragColor = vec4(colour, 1.0);
}
