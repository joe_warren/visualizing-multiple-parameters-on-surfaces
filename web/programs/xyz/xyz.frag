precision highp float;

varying vec3 position;

void main(void) {
  gl_FragColor = vec4(position* 0.0075 + 0.5, 1.0);
}
