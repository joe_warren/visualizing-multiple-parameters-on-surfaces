precision highp float;

uniform float uTexScale;
uniform float uNormalScale;

varying vec3 normal;
varying vec3 tanX;
varying vec3 tanY;
varying vec3 tanZ;

varying vec3 Onormal;

varying vec4 position;
varying vec4 Oposition; 

varying vec3 U;

varying float data;
varying float dataII;

uniform sampler2D uSamplerNormals;

#include "colour_map.h"

void main(void) {
  vec3 n = normalize( Onormal.xyz );
  vec3 blend_weights = abs( n );
  blend_weights = ( blend_weights - 0.2 ) * 7.;  
  blend_weights = max( blend_weights, 0. );
  blend_weights /= ( blend_weights.x + blend_weights.y + blend_weights.z );

  vec2 coord1 = position.yz * uTexScale;
  vec2 coord2 = position.zx * uTexScale;
  vec2 coord3 = position.xy * uTexScale;

  vec3 bump1 = texture2D( uSamplerNormals, coord1 ).rgb;  
  vec3 bump2 = texture2D( uSamplerNormals, coord2 ).rgb;  
  vec3 bump3 = texture2D( uSamplerNormals, coord3 ).rgb; 

  vec3 blended_bump = bump1 * blend_weights.xxx +  
                      bump2 * blend_weights.yyy +  
                      bump3 * blend_weights.zzz;

  vec3 blended_tangent = normalize(tanX) * blend_weights.xxx +  
                         normalize(tanY) * blend_weights.yyy +  
                         normalize(tanZ) * blend_weights.zzz; 

  float normalScale = uNormalScale;

  vec3 normalTex = blended_bump * 2.0 - 1.0;
  normalTex.xy *= normalScale;

  normalTex = normalize( normalTex );
  mat3 tsb = mat3( normalize( blended_tangent ), 
                   normalize( cross( normal, blended_tangent ) ),
                   normalize( normal ) );

  normalTex = normalize(tsb * normalTex);

  vec3 vLight = normalize( vec3( 1.0, 1.0, 1.0 ) );

  float intensity = dot( vLight, normalTex );

  intensity = 0.3 + 0.3*clamp(intensity, 0.0, 1.0);

  vec3 vHalf = normalize(  ( vLight + vec3( -Oposition ) ) );

  float specular = pow( max(dot( vHalf , normalTex ), 0.0 ), 16.0 );

  specular = (dataII>0.0?dataII:-1.0) * specular;

  vec3 colour = colour_map( data, intensity );

  colour += vec3( .7, .7, .7 ) * specular;

  gl_FragColor = vec4(colour, 1.0);
}
