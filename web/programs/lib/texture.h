vec3 getBumpMapCoord(vec3 blend_weights){

  vec2 coord1 = position.yz * uTexScale;
  vec2 coord2 = position.zx * uTexScale;
  vec2 coord3 = position.xy * uTexScale;

  vec3 bump1 = texture2D( uSamplerNormals, coord1 ).rgb;  
  vec3 bump2 = texture2D( uSamplerNormals, coord2 ).rgb;  
  vec3 bump3 = texture2D( uSamplerNormals, coord3 ).rgb; 

  vec3 blended_bump = vec3( 0.0, 0.0, 0.0 );
  if( uTriplanarBool ){
   
    vec2 coord1 = position.yz * uTexScale;
    vec2 coord2 = position.zx * uTexScale;
    vec2 coord3 = position.xy * uTexScale;

    vec3 bump1 = texture2D( uSamplerNormals, coord1 ).rgb;  
    vec3 bump2 = texture2D( uSamplerNormals, coord2 ).rgb;  
    vec3 bump3 = texture2D( uSamplerNormals, coord3 ).rgb; 
    blended_bump = bump1 * blend_weights.xxx +  
                   bump2 * blend_weights.yyy +  
                   bump3 * blend_weights.zzz;
  } else {
    blended_bump = texture2D( uSamplerNormals, uv * uTexScale ).rgb;  
  }
  return blended_bump;
}

vec3 getTexCoord(vec3 blend_weights){

  vec2 coord1 = position.yz * uStdTextureScale;
  vec2 coord2 = position.zx * uStdTextureScale;
  vec2 coord3 = position.xy * uStdTextureScale;

  vec3 blended_bump = vec3( 0.0, 0.0, 0.0 );
  if( uTriplanarBool ){
   
    vec2 coord1 = position.yz * uStdTextureScale;
    vec2 coord2 = position.zx * uStdTextureScale;
    vec2 coord3 = position.xy * uStdTextureScale;

    vec3 tex1 = texture2D( uSamplerTexture, coord1 ).rgb;  
    vec3 tex2 = texture2D( uSamplerTexture, coord2 ).rgb;  
    vec3 tex3 = texture2D( uSamplerTexture, coord3 ).rgb; 
    blended_bump = tex1 * blend_weights.xxx +  
                   tex2 * blend_weights.yyy +  
                   tex3 * blend_weights.zzz;
  } else {
    blended_bump = texture2D( uSamplerTexture, uv * uStdTextureScale ).rgb;  
  }
  return blended_bump;
}
