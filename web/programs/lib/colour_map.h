vec3 colour_map( float data, float intensity ){
  vec3 colour;
  colour.rgb = vec3(
    (1.0 - smoothstep(0.0, 0.2, data)) + smoothstep( 0.6, 0.8, data ),
    smoothstep( 0.2, 0.4, data) - smoothstep( 0.8, 1.0, data),
    1.0 - smoothstep( 0.4, 0.6, data )
  );
  colour = mix( vec3(0.0, 0.0, 0.0), colour, intensity );
  if( data < 0.0 ){
    return vec3( 0.5, 0.5, 0.5 );
  }
  return colour;
}
