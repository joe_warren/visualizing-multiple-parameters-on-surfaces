precision highp float;

uniform float uTexScale;
uniform float uStdTextureScale;

uniform sampler2D uSamplerTexture;
uniform sampler2D uSamplerNormals;

uniform bool uTriplanarBool;
varying vec2 uv;

varying vec3 normal;
varying vec3 Onormal;

varying vec4 position;

varying float data;
varying float dataII;

#include "colour_map.h"
#include "texture.h"

void main(void) {
  vec3 n = normalize( Onormal.xyz );
  vec3 blend_weights = abs( n );
  blend_weights = ( blend_weights - 0.2 ) * 7.;  
  blend_weights = max( blend_weights, 0. );
  blend_weights /= ( blend_weights.x + blend_weights.y + blend_weights.z );

  vec3 blended_tex = getTexCoord( blend_weights );
  
  float tex = (blended_tex.x + blended_tex.y + blended_tex.z)/3.0;
  tex = tex * 2.0 - 1.0;

  float intensity = dot( normalize( vec3( 1.0, 1.0, 1.0 ) ),
                         normalize( normal ) );

  intensity = 0.1 + 0.3*clamp(intensity, 0.0, 1.0);

  if( dataII > tex ){
    intensity += dataII-tex;
  }
  vec3 colour = colour_map( data, intensity );

  gl_FragColor = vec4(colour, 1.0);
}
