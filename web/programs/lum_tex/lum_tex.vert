attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute vec2 aVertexUV;
attribute float aFloatData;
attribute float aFloatDataII;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

varying vec2 uv;

varying vec3 normal;
varying vec3 Onormal;

varying vec4 position;

varying float data;
varying float dataII;

void main(void) {
  data = aFloatData;
  dataII = aFloatDataII;

  uv = aVertexUV;

  Onormal = normalize( aVertexNormal );
  normal =  normalize( mat3(uMVMatrix) * aVertexNormal );

  position = vec4( aVertexPosition, 1.0 );
  gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
}

