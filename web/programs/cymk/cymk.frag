precision highp float;

varying vec3 normal;

varying float data;
varying float dataII;

uniform sampler2D uSamplerNormals;

vec3 cymk2rgb( float c, float y, float m, float k ){
  vec3 rgb;
  rgb.r = (1.0-c) * (1.0-k);
  rgb.g = (1.0-m) * (1.0-k);
  rgb.b = (1.0-y) * (1.0-k);
  return rgb;
}


void main(void) {
  vec3 n = normalize( normal );
  float intensity = max( 0.0, dot( n, normalize( vec3(1.0, 1.0, 1.0) ) ) );
  intensity = intensity *0.6 + 0.4;
  vec3 colour = cymk2rgb( 0.0, data, dataII, 1.0-intensity );
  if( data < 0.0 || dataII <0.0 ){
    discard;
  } 
  gl_FragColor = vec4(colour, 1.0);
}
