#include "derivatives.h"

precision highp float;

varying vec3 normal;
varying vec3 position;

varying float data;
varying float dataII;

uniform sampler2D uSamplerNormals;

#include "colour_map.h"

float round_intensity( float i ){
 return 1.0;
 return exp2( floor( log2( (1.0-i)*120.0 + 10.0) ) )/120.0;
}

float single_axis_lines( float p, float ri ){
  float f  = fract ( p *ri );
  float df = fwidth( p ) *ri;
  float g = smoothstep(0.0, df*0.25, f) *
    smoothstep(df * 1.75, df*2.0, f);
  return g;
}
float hatching_amount( vec3 p, float ri){
  return min( min( 
    single_axis_lines( p.x,ri ),
    single_axis_lines( p.y,ri )),
    single_axis_lines( p.z,ri ));
}

void main(void) {
  vec3 n = normalize( normal );
  float intensity = max( 0.0, dot( n, normalize( vec3(1.0, 1.0, 1.0) ) ) );
  vec3 colour = mix( 
    colour_map( data, dataII*0.5 + 0.1 ),
    vec3( 1.0, 1.0, 1.0 ),
    hatching_amount( position, round_intensity( intensity ) 
  ) );
  gl_FragColor = vec4(colour, 1.0);
  if( data < 0.0 || dataII <0.0 ){
    gl_FragColor = vec4(0.2, 0.2, 0.2, 1.0)*intensity+vec4(0.3, 0.3, 0.3, 1.0);
  } 
}
