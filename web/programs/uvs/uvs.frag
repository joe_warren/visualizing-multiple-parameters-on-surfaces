precision highp float;

varying vec2 uv;

void main(void) {
  gl_FragColor = vec4(uv*0.05 + 0.5,1.0, 1.0);
}
