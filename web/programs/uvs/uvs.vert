attribute vec3 aVertexPosition;
attribute vec3 aVertexNormal;
attribute float aFloatData;
attribute float aFloatDataII;
attribute vec2 aVertexUV;

varying vec2 uv;

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;

void main(void) {
  uv = aVertexUV;
  gl_Position = uPMatrix * uMVMatrix * vec4(aVertexPosition, 1.0);
}

