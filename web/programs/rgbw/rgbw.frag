precision highp float;

varying vec3 normal;

varying float data;
varying float dataII;

void main(void) {
  vec3 n = normalize( normal );
  float intensity = max( 0.0, dot( n, normalize( vec3(1.0, 1.0, 1.0) ) ) );
  intensity = intensity *0.4;
  vec3 colour = vec3( data, 0.0, dataII ) + intensity * vec3(1.0, 1.0, 1.0);
  if( data < 0.0 || dataII <0.0 ){
    discard;
  } 
  gl_FragColor = vec4(colour, 1.0);
}
