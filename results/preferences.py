#!/usr/bin/env python
import sys
import json

from scipy.stats import f_oneway

testName = "Click and drag to order these images by how you liked each visualization"
count = 0
programNames = ["lightspots","contours","bumpmap","cymk"]
counts = dict()
arrays = dict()
 
for n in programNames:
   counts[n] = [0,0,0,0]
   arrays[n] = []
for arg in sys.argv[1:]:
    obj = json.load( open(arg) );
    for i in obj:
        if i["test"] == testName:
            o = i["order"]
            o = o[:-4] # trim trailing []=1
            l = o.split("[]=1&")
            for n in programNames:
                counts[n][l.index(n)] +=1
                arrays[n].append( 3-l.index(n) )
for n in programNames:
  score = counts[n][0]*3 + counts[n][1]*2 + counts[n][2]*1
  print n + ": " + str(counts[n]) + " score: " + str(score) 

print f_oneway( arrays["bumpmap"], arrays["lightspots"], arrays["cymk"],arrays["contours"]  )

