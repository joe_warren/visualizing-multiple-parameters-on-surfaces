#!/usr/bin/env python
import sys
import json

from scipy.stats import f_oneway

testName = "How clear do you find this visualization?"

programNames = ["luminescent texture","contours","bumpmap","cymk"]
total = 0
average = dict()
counts = dict()
arrays = dict()

for n in programNames:
   average[n] = 0
   counts[n]=[0,0,0,0,0]
   arrays[n]=[]
for arg in sys.argv[1:]:
    total += 1
    obj = json.load( open(arg) );
    for i in obj:
        if i["test"] == testName:
            v = i["value"]
            average[i["program"]] += v
            counts[i["program"]][v] += 1
            arrays[i["program"]].append(v)
for n in programNames:
  print n + ": " + str(counts[n]) + ": " + str(float(average[n])/total)

print f_oneway( arrays["bumpmap"], arrays["luminescent texture"], arrays["cymk"],arrays["contours"]  )
print f_oneway( arrays["bumpmap"]+ arrays["luminescent texture"]+ arrays["cymk"],arrays["contours"]  )

