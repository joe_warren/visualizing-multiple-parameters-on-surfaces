#!/usr/bin/env python
import sys
import json

count = 0
for arg in sys.argv[1:]:
    obj = json.load( open(arg) );
    for i in obj:
        if i["test"] == "Do you have any comments on the visualizations?":
            if i["value"] != "":
                 print i["value"]
                 print "================================="
                 count += 1

print str(count) + " total comments"
