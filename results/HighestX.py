#!/usr/bin/env python
import sys
import json
import math

from scipy.stats import f_oneway

testName = "Click the point on the object with the highest X Axis measurement"

programNames = ["luminescent texture","contours","bumpmap","cymk"]
total = 0
average = dict()
counts = dict()
arrays = dict()

for n in programNames:
   arrays[n] = []

def distance( a, b ):
    sum = 0
    for i in [0,1,2]:
         sum += math.pow(a[i]-b[i],2)
    return math.sqrt(sum)

def score( obj ):
   return distance( obj["click_position"], obj["correct_value"]["pos"] )

for n in programNames:
   average[n] = 0
for arg in sys.argv[1:]:
    total += 1
    obj = json.load( open(arg) );
    for i in obj:
        if i["test"] == testName:
            average[i["program"]] += score(i)
            arrays[i["program"]].append(score(i))

for n in programNames:
  print n + ": " + str(float(average[n])/total)
print f_oneway( arrays["luminescent texture"], arrays["bumpmap"],arrays["cymk"],arrays["contours"]  )

print f_oneway( arrays["luminescent texture"]+ arrays["bumpmap"],arrays["cymk"],arrays["contours"]  )
