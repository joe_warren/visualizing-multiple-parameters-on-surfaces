#!/usr/bin/env python
import sys
import json
import math

from scipy.stats import f_oneway
import numpy as np 

testName = "Click the point with the faintest visible circle (circles drawn using the Y axis measurement)"

modelChoices = {1:"centers/lf_centers.js", 2:"centers/v_centers.js"}

programNames = ["luminescent texture","contours","bumpmap","cymk"]

def distance_squared( a, b ):
    sum = 0
    for i in [0,1,2]:
         sum += math.pow(a[i]-b[i],2)
    return sum

total = 0
average = dict()
counts = dict()
arrays = dict()

for n in programNames:
   average[n] = 0
   arrays[n]=[]
for arg in sys.argv[1:]:
    total += 1
    obj = json.load( open(arg) );
    for i in obj:
        if i["test"] == testName:
            pos = i["click_position"]
            model = modelChoices[i["model"]]
            m = json.load(open(model))
            dists = [distance_squared(pos,a) for a in m["centers"] ]
            score = float( dists.index(min(dists)) )/len(dists) 
            average[i["program"]] += score
            arrays[i["program"]].append(score)
            if not score in counts:
               counts[score] = 0
            counts[score] += 1

print "higher score is better" 

for n in programNames:
   print n + ": " + str(counts)
for n in programNames:
   print n + ": " + str(average[n]/total)
print f_oneway( arrays["luminescent texture"],arrays["contours"] ,arrays["bumpmap"], arrays["cymk"] )
print f_oneway( arrays["luminescent texture"] + arrays["contours"] + arrays["bumpmap"], arrays["cymk"] )
print f_oneway( arrays["bumpmap"], arrays["cymk"] )
