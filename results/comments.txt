It might be nice to have some feedback at the end commenting on how you had performed
=================================
Hard to separate effect of axes for CMYK, much easier with bumpmap and lightspots. Not sure about this, but what would the effect be if (in lightspots) the spot size was kept constant, with an outer ring which varied black-white along the X axis.

Much more far fetched, would some kind of small lines, which vary colour along one axis, and angle along the other be of any use. 
=================================
They all don't seem terribly good to me... The 'rainbow' color gradient is not the best (see e.g. http://www.poynter.org/uncategorized/224413/why-rainbow-colors-arent-always-the-best-options-for-data-visualizations/). Also, the questions are confusing - I get asked about faintest and smallest circle both for X and Y measurement, but it is X-size and Y-faintness (say), and for some visualizations that doesn't make any sense at all. So in the end I got frustrated with these and just clicked random points to keep going...
=================================
I have no idea what you mean by "circle" in all of these tests. It was extremely frustrating. I wanted to quit but thought atleast a complete dataset could help.

The best viz was the one that had colour in the Y axis... the 3d-dots in the x-axis seemed too confusing because I didn't know if it was dependent on the camera's viewing angle or the point-source light.
The grayscale contour lines seemed like an improvement, but maybe I couldn't tell the difference so easy. Maybe try banding the gray colours into groups instead of a continuous shade.

The saturation/colour one in the y-axis was too hard to tell, I wouldn't use it. I think it takes too much of a discerning eye to spot the differences from a "high" red to a "low" pink.
=================================
Quite difficult to understand the data. Would take some practise to get used to this, I feel.
=================================
nice visualizations. very attractive. good luck with the project! :)
=================================
I was unclear what you meant by circles. I think the survey needs more explanation.
=================================
Bumpmap x-axis was really hard to see changes in. Perhaps varying the shapes as well as intensity would make it clearer?
=================================
hi joe love from hafiz and evan
=================================
The last one has far too thick a mesh - really hard to see any detail underneath it.
=================================
Why not use grayscale on x and rainbow on y instead of using yelllow-pink. The black outlines of dots is really distracting and defeats the purpose of a heatmap if half of it is colored black.



It would help if the scale went from red to green or blue instead of back to a hue similar to red. Contrast to define ends is important.


=================================
The contour map has the clearest resolution in the x-axis. Would prefer to have the contour values always visible. And more contours if available.

The other techniques have very poor resolution in x-axis. 
The bump map felt kinda binary in distinguishing x-values (glint, no glint)
The light spots were clearer in showing different x-values.
The mix of colours is horribly confusing.
=================================
More context would be helpful in understanding the visualizations.
=================================
I am not sure what they are
=================================
Didn't fully understand some of the questions, there did not appear circles on some visualisation. The X axis colours on the key dissapeared during the survay and left just coloured bars with the Y value.
=================================
With the vertebrae maybe rescale the colours. It was had to see any differentiation between values until the contours were added. 

The bump map and light 'something or other' map are extremely sore on the eyes to look at. Also confusing as there's a bit of the 'blue-black/white-gold dress' phenomena when yours eyes continually alternate between seeing the bumps as facing inwards or outwards, making it harder to focus. I think for these the subtlety in the bump gradients is hard to figure out. My eyes seemed to interpret it as areas which were more well-lit rather than less bumpy.

If the contour plots didn't have the numbers overlaying during the visualisation they'd be awesome :)
=================================
It wasn't at all clear what the circles were so I guess all my feedback is useless. I think the instructions need to be _much_ clearer. Nice webpage though!
=================================
Sometimes it was telling me to look for circles and there were no visible circles. I don't know if the correct data representation failed to load or what.
=================================
The Yellow/Magenta/Red one is nearly impossible for me to distinguish, I can't separate the colours out as two distinct sets of information, my mind is telling me I'm looking at a single spectrum.

The bumpmap one is hard too, I feel like my brain is telling me that the texture should be constant for the whole object, it's almost like battling against an optical illusion to see what's really there.

The lightspots one was easiest. I don't have too much to say about the contours, it's ok to find regions but when you take a random point between contours it's hard to say where it would be on the X axis of the graph.

Have you considered a sinusoidal wave pattern/texture over the object that increases in frequency relative to one of the axes? Straight line as the 0 etc.

Anyway, it's cool. I like innovative ways of displaying information.

=================================
19 total comments
