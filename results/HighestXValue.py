#!/usr/bin/env python
import sys
import json
import math

from scipy.stats import f_oneway

testName = "Click the point on the object with the highest X Axis measurement"

programNames = ["luminescent texture","contours","bumpmap","cymk"]
total = 0
average = dict()
counts = dict()
arrays = dict()

for n in programNames:
   average[n] = 0
   arrays[n] = []

for arg in sys.argv[1:]:
    total += 1
    obj = json.load( open(arg) );
    for i in obj:
        if i["test"] == testName:
            if not "value" in i:
                print arg
            average[i["program"]] += i["value"]
            arrays[i["program"]].append(i["value"])

for n in programNames:
  print n + ": " + str(float(average[n])/total)

print f_oneway( arrays["luminescent texture"], arrays["bumpmap"],arrays["cymk"],arrays["contours"]  )

