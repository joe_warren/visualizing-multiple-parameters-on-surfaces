

function setup( ) {
  anchors = $( ".anchor" );
  anchorIndex = 0;
  $('html, body').animate({
    scrollTop: $(anchors[anchorIndex]).offset().top 
  }, 1000);

  if( anchors.length !== 0 ){
    window.addEventListener("keypress", function(e){
      if( e.key === "Right" ){
        anchorIndex++;
        window.console.log();
        if( anchorIndex >= anchors.length ){
          anchorIndex = 0;
        }
      }
      if( e.key === "Left" ){
        anchorIndex--;
        if( anchorIndex < 0 ){
          anchorIndex = anchors.length - 1;
        }
      }
      if( e.key === "Left" || e.key === "Right" ){
        $('html, body').animate({
          scrollTop: $(anchors[anchorIndex]).offset().top 
        }, 1000);
      }
    
    } );
  }
}
window.onload = setup; 

