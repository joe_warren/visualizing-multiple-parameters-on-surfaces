\documentclass[a4paper,12pt]{article}

\usepackage{a4wide}

\usepackage[a4paper, margin=2.5cm]{geometry}
%\geometry{textwidth=0.8\paperwidth,
%         textheight=0.8\paperheight}

%\setlength{\headsep}{5pt}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{array}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{listings}
\usepackage{fixltx2e}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{multirow}
\usepackage[runin]{abstract}


\usepackage{color}
\usepackage[usenames,dvipsnames]{xcolor}
\definecolor{bggray}{rgb}{0.95,0.95,0.99}

\usepackage{tikz}
\usepackage{gensymb}

\renewcommand{\abstractname}{Summary: } 
\renewcommand{\absnamepos}{empty}


\usepackage{titling}
\usepackage{blindtext}

\setlength{\droptitle}{-4em}     % Eliminate the default vertical space
\addtolength{\droptitle}{-2em}   % Only a guess. Use this for adjustment

\newlength{\imglength} 
\setlength{\imglength}{0.4\textwidth}

\usepackage{enumitem}

\begin{document}

\title{\large\textsc{Multi Parameter Visualization on a 3D Surface}\\ Technical Milestone Report\vspace{-1em}}
\author{Joseph Warren\;\;::\;\;Churchill College}
\date{}
\maketitle
\vspace{-5em}
\begin{abstract}
This report documents the design of multiple techniques for displaying multiple sets of surface data on a single image.
Four such techniques are described, these include:
\begin{itemize}[noitemsep,nolistsep]
\item A technique based on the CYMK colour model.
\item Representing a parameter using bright dots of varying size and intensity.
\item Using bump mapping to represent a parameter by varying the surface texture.
\item The use of contours.
\end{itemize}
A survey is planned in order to judge the effectiveness of these techniques.
\end{abstract}
\vspace{0.5em}
\hrule
\vspace{0.2em}
\hrule

\section{Project Scope}
The aim of this project is to investigate different ways of plotting multiple sets of data that vary over the surface of an object.

Broadly, there are two applications for this.
It may be desirable to display several different types of data on a single object.
An example could be the thickness of a bone's cortex, and the density of the trabecular bone underneath the surface.
Similarly finite element analysis produces several datasets that could be displayed on one image\cite{fea}.

Additionally, two different specimens of the same type of data could be registered onto a single surface, and displayed in a single rendering.
For instance, given bone scans from a patient with one injured and one healthy femur, data from the healthy femur could be mapped onto the injured one.
Plotting these data in the same image could highlight differences.

Data relevant to this project includes any data set that can be plotted on an object's surface.
This implies data that relates to the surface, such as the thickness of a bone cortex, as well as data that is has values at all points within an object but can also be displayed on the surface, such as stresses and strains calculated through finite element analysis.

\section{Prior Work}

It is usual, when rendering a 3D object, that the brightness of the surface is varied to imitate the effects of lighting, and thus indicate the objects shape.
Because of this, it is common to plot surface data by varying the hue\cite{neuron,cortex}.
This works because the brightness and the hue of an colour are independent of one another\cite[~p.192]{ImgProc}.
A colour's hue is a single dimensional parameter, and therefore does not lend itself to displaying multiple datasets.

A previous attempt to visualize several datasets in the same image involved rendering a surface and varying the hue to represent one parameter, and then rendering a second translucent surface just outside the surface of the object.
The height of the second surface above the first one can be varied to show the second dataset.
This has some attractions when the second dataset represents surface thickness, such as the thickness of a bone cortex.
The technique is limited, since the displacement between the inner and outer surface is most apparent at the sides of the object facing at right angles to the view angle.
By definition, these are not the parts of the object that the view is centered on.
An example of a femur rendered as two layers with the displacement showing cortical thickness is provided in figure \ref{fig:two_layer}.

Other techniques have involved varying the saturation of the colour on the surface.
This can be effective for showing binary data: data about a surface point that can be either true or false, such as whether the first dataset is statistically significant at a point on the surface, an example of this is provided in figure \ref{fig:binary}.
Unfortunately lowering a colour's saturation makes it more difficult to tell the different hues apart\cite{neuron}.
When the saturation is zero, all possible hue values produce the same colour \cite[~p.193]{ImgProc}.

\section{Technologies Used}
A WebGL program was written to explore different visualization techniques.
WebGL is derived from OpenGL ES 2.0, and allows for graphics programs to be written that run in a web browser.
The motivation for using WebGL was to make the program as portable as possible (It should be possible to run it on any platform with a modern web browser) while allowing the program to take full advantage of the GPU, thus allowing for rendering at interactive frame rates.

Each visualization technique was implemented as a shader program written in the ``OpenGL Shading Language''\cite{glsl}.
Shader programs control the programmable graphics pipeline. 
This allows for a large range of rendering techniques to be efficiently employed.

The program loads surface data encoded using a JSON format.

The Arcball rotation control technique described in \cite{arcball} is implemented, allowing for the object visualized to be rotated by the user.
This is possible because of the small rendering times. 
This technique uses quaternions (an extension of complex numbers) to represent rotations.
Quaternions are useful for representing rotation because unlike Euler angles, they do not exhibit gimbal lock: a situation where an object can be rotated into positions limiting the possible axes of rotation.
  
\section{Approaches Tried}

\subsection{CYMK Shader}

As the human eye contains three types of cone cell, the set of all colours can be thought of as a three dimensional space\cite[~p.193]{ImgProc}.
Despite this, colour printers commonly use a 4 dimensional colour model called CYMK\cite[~p74]{cymk}.
In the CYMK model colours consist of the combination of three subtractive primary colours: cyan, magenta and yellow along with black (or key).
The ``vectors'' that define CYMK space are not orthogonal: cyan magenta and yellow can be combined in equal quantities produce blacks and greys.

One method for displaying several datasets was inspired by the CYMK colour model.
We can use the black channel in CYMK space for shading, representing the shape of the object. Then we can use two of the primary colour channels to represent one dataset each.
This technique is analogous to taking a white object, and applying a different colour ink to it in amounts proportional to the value of the data at each point on the surface.

A vertebrae rendered using this technique is shown in figure \ref{fig:cymk}.

One of the benefits of this visualization technique is that it is directly analogous to a physical process.
It also displays both datasets in the same way, neither dataset is significantly clearer or less clear than the other. 
The spatial resolution of the data is very high, however it takes relatively large changes in the value of the data for a distinguishable change in the rendering; the value resolution of the data is low, especially when compared to representing data using the hue.

\subsection{Light Dot Shader}

Certain possibilities for representing multiple datasets are opened up if we are able to map a repeating texture onto the surface.
This can be done by manually creating UV texture coordinates for the surface to be represented.
Manual UV generation requires a large amount of human effort and would be unsuitable for most applications of these rendering techniques.
An alternative to manual UV generation is to use triplanar projection.
This involves projecting a texture onto the surface from three different orthogonal directions, and then mixing these textures in proportion to the extent that the face was facing direction of projection\cite{triplanar}.

Another alternative is automatic texture coordinate generation, using the technique described in \cite{conformal}.
Rather than implementing this, texture coordinates were generated using the Blender 3D modeling package; Blender supports this method for UV generation.

A visualization technique was consisted of mapping a repeating texture of blurry dots onto the surface.
The first dataset was represented by the hue, and the brightness varied to suggest shape.
At points on the image where the second dataset was larger than the texture value (generally close to the center of the dots), the surface brightness was increased by the difference between the second data value and the texture value.
The result of this is to produce large bright dots on areas of the object where the dataset is large, and smaller dimmer ones where it is less large.
This looks a little bit like the object has been illuminated with LED lights embedded just below the surface.

When triplanar projection is used, the distribution of the dots becomes sparser at points on the surface that are not aligned with any of the projection angles.

When using the conformal mapping technique described in \cite{conformal}, discontinuities appear in the pattern of the dots between patches that map to different regions in texture space.
This can be observed in figure \ref{fig:light}.

The key strength of this visualization technique is that it uses hue to represent the first dataset and should look familiar to anyone used to seeing data represented like this.
A disadvantage of the technique is that the resulting image looks somewhat unnatural.
Additionally the spatial resolution of the second parameter is limited by the frequency of the dots.

\subsection{Bumpmap Shader}
The brightness of every point on the surface is computed using the angle between a lighting direction and the normal to the surface\cite[~p149]{glsl}.
This common shading technique can be extended by a method known as bump mapping, this involves modulating the surface normal by a vector stored in a texture, known as a normal map.
The motivation for this is to simulate surface detail beyond the object's mesh resolution.

A visualization technique was devised that involves applying bump mapping, but varying the perturbation of the normal in proportion to the value of the second dataset.
This has the effect of making the surface appear rough where the data value is large, and smooth where it is not.
Since this only affects the brightness the first dataset can still be displayed using the hue.
The nature of the roughness can be changed by using different normal maps.
Figure \ref{fig:bump} shows a femur where the normal map was generated from a repeating pattern of dots.
Using a normal map derived from a noise function such as Perlin noise \cite{perlin} can remove the visual artefacts produced by triplanar projection blending two textures together.

This technique is strong in that it retains similarity to single parameter hue based visualization.
It also has a natural analogue as it looks like a rough version of the object.
This metaphor is particularly appealing when the second dataset is a thickness, as the height of the surface deformations varies in proportion to that thickness.

As with the ``light dot'' technique, the spatial resolution of the second parameter is limited by the frequency of the texture.
Additionally the value resolution of the second parameter is limited to the number of different levels of bumpiness that the eye can discern.

\subsection{Contour Shader}

A shader was written that represented the second dataset by rendering contours on the surface.
The shader uses the \texttt{OES\_standard\_derivatives}\cite{derivatives} extension to GLSL to make the contour lines fixed width in screen space.
The contours are drawn by altering every pixel where the second parameter lies within certain ranges.
The contour ranges are chosen to be evenly spaced to produce a fixed number of contours, and to have a width proportional to \texttt{abs(dFdx(contour)) + abs(dFdy(contour))} where \texttt{contour} is the second parameter.
In GLSL this equation is implemented as the \texttt{fwidth} function.

Individual contours can be labelled to make reading values easier.

An image rendered by the contour shader is shown in figure \ref{fig:contour}.

Using contours to represent a second dataset is effective since it provides good spatial resolution, and the data resolution can be as high as the number of contours.
In addition to this, people are familiar with the idea of using contours to represent data.

\section{Remaining Work}
The effectiveness of the techniques described above at representing surface data is subjective. 
Different shading methods may be more effective for different applications.
In order to find out what the most effective of the techniques is, we are planning to run a survey.
Since the software runs in a web browser, it has potential to be converted to an interactive online survey.

Therefore the public can be surveyed, the survey should be accessible to a wide range of people in order to get as large a sample size as possible.

As well as judging which of the techniques is most effective, we also want to quantify the spatial and value resolutions that each type of visualization displays data to.
This could be investigated using a technique similar to those used to set colour correction in printers, where the difference between two values is decreased until the user is unable to distinguish between them. 

I anticipate the survey being deployed in the first few weeks of the term to maximise the amount of time that it can be run for. 

\newpage
\section{Figures}
\begin{figure}[H]
    \centering

    \begin{minipage}{0.45\textwidth}
        \centering
        \includegraphics[height=0.7\linewidth]{img/two_layer.png}
        \caption{Two Layers, layer displacement indicates surface thickness}
        \label{fig:two_layer}
    \end{minipage}\hspace{0.05\textwidth}%
    \begin{minipage}{0.45\textwidth}
        \centering
        \includegraphics[height=0.7\linewidth]{img/binary.png}
        \caption{Saturation set high or low to indicate statistical significance}
        \label{fig:binary}
    \end{minipage}
\vspace{2em}

    \begin{minipage}{0.5\textwidth}
        \centering
        \includegraphics[height=0.7\linewidth]{img/cymk.png}
        \caption{CYMK Shader}
        \label{fig:cymk}
    \end{minipage}%
    \begin{minipage}{0.5\textwidth}
        \centering
        \includegraphics[height=0.7\linewidth]{img/light_spots.png}
        \caption{Light Dot Shader}
        \label{fig:light}
    \end{minipage}
\vspace{2em}

    \begin{minipage}{0.5\textwidth}
        \centering
        \includegraphics[height=0.7\linewidth]{img/dotted.png}
        \caption{Bumpmap Shader}
        \label{fig:bump}
    \end{minipage}%
    \begin{minipage}{0.5\textwidth}
        \centering
        \includegraphics[height=0.7\linewidth]{img/contours.png}
        \caption{Contour Shader}
        \label{fig:contour}
    \end{minipage}
\end{figure}


\bibliographystyle{plain}
\bibliography{report}

\end{document}
