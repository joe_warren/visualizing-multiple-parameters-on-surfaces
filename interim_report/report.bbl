\begin{thebibliography}{10}

\bibitem{neuron}
Elena A. et~al Allen.
\newblock Data visualization in the neurosciences: Overcoming the curse of
  dimensionality.
\newblock {\em Neuron}, 74(4):603--608, May 2012.

\bibitem{cymk}
Mark Galer and Les Horvat.
\newblock {\em Digital Imaging: Essential Skills}.
\newblock Focal, 2003.

\bibitem{triplanar}
Ryan Geiss.
\newblock Generating complex procedural terrains using the gpu.
\newblock In Hubert Nguyen, editor, {\em GPU Gems 3}. Addison-Wesley, 2007.

\bibitem{ImgProc}
Rafael~C. Gonzalez and Paul Wintz.
\newblock {\em Digital Image Processing}.
\newblock Addison-Wesley, Reading, Massachusetts, second edition, 1987.

\bibitem{arcball}
P.S. Heckbert.
\newblock {\em Graphics Gems IV}.
\newblock The Graphics gems series. AP Professional, 1994.

\bibitem{fea}
Noboru Kikuchi.
\newblock {\em Finite Element Methods in Mechanics}.
\newblock Cambridge University Press, 1995.

\bibitem{conformal}
Bruno L{\'e}vy, Sylvain Petitjean, Nicolas Ray, and J{\'e}rome Maillot.
\newblock Least squares conformal maps for automatic texture atlas generation.
\newblock {\em ACM Trans. Graph.}, 21(3):362--371, July 2002.

\bibitem{perlin}
Ken Perlin.
\newblock Improving noise.
\newblock In {\em ACM Transactions on Graphics (TOG)}, volume 21, 3, pages
  681--682. ACM, 2002.

\bibitem{derivatives}
Yakov Rekhter and Tony Li.
\newblock {OES\_standard\_derivatives}.
\newblock {OpenGL ES Extension}~45, {Khronos Group}, July 2007.

\bibitem{glsl}
Randi~J. Rost.
\newblock {\em OpenGL(R) Shading Language ($2^{nd}$ Edition)}.
\newblock Addison-Wesley Professional, 2005.

\bibitem{cortex}
GM~Treece, AH~Gee, PM~Mayhew, and KES Poole.
\newblock High resolution cortical bone thickness measurement from clinical ct
  data.
\newblock {\em Medical Image Analysis}, 14(3):603--608, jan 2010.

\end{thebibliography}
